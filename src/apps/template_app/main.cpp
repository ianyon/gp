#include <QtGui/QApplication>
#include "qt_interface/qt_template_app_interface.h"
#include "core/template_app_core.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //TemplateAppCore is independent of graphic interface platform:
    TemplateAppCore core;

    //Qt graphic interface for template app.
    QtTAInterface interface(&core);

    //Pointer assignment
    core.set_interface(&interface);
    core.start();

    //Show graphic interfce:
    interface.show();
    return app.exec();
}
