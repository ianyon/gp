#ifndef _TEMPLATE_APP_CORE_H_
#define _TEMPLATE_APP_CORE_H_

#include "../template_app_generic_interface.h"

class TemplateAppCore
{
    public:
        TemplateAppCore(){};
        ~TemplateAppCore(){};

        void set_interface(TAGenericInterface* graphicInterface_);

        TAGenericInterface* get_interface();

        void start();

    private:
        TAGenericInterface *m_graphic_interface;
};

#endif //_TEMPLATE_APP_CORE_H_
