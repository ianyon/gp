#include "template_app_core.h"
#include <iostream>

using namespace std;

void TemplateAppCore::set_interface(TAGenericInterface* graphicInterface_)
{
    m_graphic_interface = graphicInterface_;
}

TAGenericInterface* TemplateAppCore::get_interface()
{
    return m_graphic_interface;
}

void TemplateAppCore::start()
{
    cout<<"TemplateAppCore started!"<<endl;
}
