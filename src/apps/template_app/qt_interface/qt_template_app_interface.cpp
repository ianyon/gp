#include "qt_template_app_interface.h"

QtTAInterface::QtTAInterface(TemplateAppCore *core, QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent,flags)
    , TAGenericInterface(core)
{
    ui.setupUi(this);

    create_qt_connections();
}

QtTAInterface::~QtTAInterface()
{
    //core->FinishSignal();
}

void QtTAInterface::update()
{

}

void QtTAInterface::create_qt_connections()
{

}
