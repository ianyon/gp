#ifndef _TA_GENERIC_INTERFACE_H_
#define _TA_GENERIC_INTERFACE_H_

class TemplateAppCore;

class TAGenericInterface
{
    public:
        TAGenericInterface(TemplateAppCore *core):m_core(core){};

        virtual ~TAGenericInterface(void){};

        virtual void update()=0;

        TemplateAppCore *m_core;
};

#endif
