#include "regression_app_core.h"
#include <iostream>

using namespace std;

RegressionAppCore::RegressionAppCore()
{
    m_gp.set_cov_func(boost::shared_ptr<SquaredExponential>(new SquaredExponential()));
    m_gp.set_mean_func(boost::shared_ptr<ZeroMean>(new ZeroMean()));
}

void RegressionAppCore::set_interface(RAGenericInterface* graphicInterface_)
{
    m_graphic_interface = graphicInterface_;
}

RAGenericInterface* RegressionAppCore::get_interface()
{
    return m_graphic_interface;
}

void RegressionAppCore::set_params(std::vector<double> const& params)
{
    return m_gp.set_hyper_params(params);
}

std::vector<double> RegressionAppCore::get_params()
{
    return m_gp.get_cov_func()->get_hyper_params();
}

void RegressionAppCore::set_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
)
{
    m_gp.set_training_data(data,labels);
}

void RegressionAppCore::add_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
)
{
    m_gp.append_training_data(data,labels);
}

Eigen::MatrixXd RegressionAppCore::get_data()
{
    return m_gp.get_training_data();
}

Eigen::VectorXd RegressionAppCore::get_labels()
{
    return m_gp.get_training_labels();
}

void RegressionAppCore::set_noise_std(
        double const&  noise_std
)
{
    m_gp.set_noise(noise_std);
}

double RegressionAppCore::get_noise_std()
{
    return m_gp.get_noise();
}

void RegressionAppCore::set_grad_calculation_internal(bool const& policy)
{
    m_nlopt.set_grad_calculation_internal(policy);
}

bool RegressionAppCore::has_derivatives()
{
    return m_gp.get_cov_func()->has_derivatives();
}

GPResult RegressionAppCore::inference(
        Eigen::MatrixXd const&          data
)
{
    m_gp.set_query_points(data);
    return m_gp.inference();
}

void RegressionAppCore::learn_params()
{
     m_nlopt.optimise(m_gp);
}

void RegressionAppCore::start()
{

}

void RegressionAppCore::clear_training_data()
{
    m_gp.clear_training_data();
}
