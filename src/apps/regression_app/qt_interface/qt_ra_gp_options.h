#ifndef _QT_RA_GP_OPTIONS_H_
#define _QT_RA_GP_OPTIONS_H_

#include <apps/regression_app/core/regression_app_core.h>
#include <QtGui/QDockWidget>
#include <string>
#include "ui_qt_ra_gp_options.h"

class QtRAInterface;

class QtRAGPOptions
    : public QDockWidget
{
    Q_OBJECT

    public:
        QtRAGPOptions(QWidget *parent = 0, Qt::WFlags flags = 0);
        ~QtRAGPOptions();

        void update();
        void set_goal_function_value(double value);
        void create_qt_connections();
        QtRAInterface *m_interface;

        unsigned int get_selected_cov_func();

        Eigen::MatrixXd get_inference_domain();
        void get_regression_domain_params(unsigned int &number_of_divisions_per_dim,double &min,double &max,double &resolution);

    signals:
        void update_signal();

    public slots:
        void change_cov_function_type(QString new_type);
        void modify_noise();
        void change_selected_hyper_param(QListWidgetItem *current,QListWidgetItem *previous);
        void modify_hyperparameter();
        void fill_params_list(std::vector<double> params);
        void optimise();
        void inference_mode_changed(int new_mode);
        void inference_domain_changed();
    private:
        Ui::QtRAGPOptions ui;

        double m_resolution;
        double m_min;
        double m_max;
};

#endif //_QT_RA_GP_OPTIONS_H_
