#include <apps/regression_app/qt_interface/qt_ra_data_manager.h>
#include <apps/regression_app/qt_interface/qt_regression_app_interface.h>
#include <iostream>
#include <fstream>
#include <vector>

#include <QFileDialog>

using namespace std;

QtRADataManager::QtRADataManager(QWidget *parent, Qt::WFlags flags)
    : QDockWidget(parent,flags)
    , m_interface(NULL)
{
    ui.setupUi(this);

    ui.y_add_line->setValidator(new QDoubleValidator(this));

    create_qt_connections();
}

QtRADataManager::~QtRADataManager()
{

}

void QtRADataManager::update()
{

        fill_data_inspection(m_interface->get_data(),m_interface->get_targets());
}

void QtRADataManager::create_qt_connections()
{
//    connect(ui.cov_func_type_combo_box,SIGNAL(currentIndexChanged(QString)),this,SLOT(change_cov_function_type(QString)));
    connect(ui.x_list_view,SIGNAL(currentRowChanged(int)),this,SLOT(change_selected_data_point_x(int)));
    connect(ui.y_list_view,SIGNAL(currentRowChanged(int)),this,SLOT(change_selected_data_point_y(int)));
    connect(ui.open_file_button,SIGNAL(pressed()),this, SLOT(open_file()));
    connect(ui.save_as_button,SIGNAL(pressed()),this,SLOT(save_as()));
    connect(ui.save_button,SIGNAL(pressed()),this,SLOT(save()));
    connect(ui.add_button,SIGNAL(pressed()),this,SLOT(add_new_data()));
    connect(ui.clear_data_button,SIGNAL(pressed()),this,SLOT(clear_data()));
}

void QtRADataManager::open_file()
{
    QFileDialog *fileDialog = new QFileDialog(this,"Open GP Raw Data","",tr("Data Files (*.dat)"));
    fileDialog->setFileMode(QFileDialog::AnyFile);
    if(fileDialog->exec())
    {
        QStringList fileName = fileDialog->selectedFiles();
        std::string name = fileName[0].toAscii().data();
        Eigen::MatrixXd data;
        Eigen::VectorXd targets;
        if(open_gp_data(name,data,targets))
        {
            m_interface->set_data(data,targets);
            ui.path_edit->setText(QString(name.c_str()));
        }
    }
}

void QtRADataManager::add_new_data()
{
    if(ui.x_add_line->text() != "" && ui.y_add_line->text() != "")
    {
        Eigen::MatrixXd newX;
        QString x_string = ui.x_add_line->text();
        QStringList string_list = x_string.split(",",QString::SkipEmptyParts);
        newX.resize(1,string_list.size());
        for(unsigned int i = 0; i < (unsigned int)string_list.size();i++)
        {
            newX(0,i) = string_list[i].toDouble();
        }
 //       newX(0,0) = x_string.toDouble();

        Eigen::VectorXd newY(1);
        QString y_string = ui.y_add_line->text();
        newY(0) = y_string.toDouble();

        m_interface->add_data(newX,newY);
    }
}

void QtRADataManager::clear_data()
{
	Eigen::MatrixXd newX = Eigen::MatrixXd::Zero(0,0);
	Eigen::VectorXd newY = Eigen::VectorXd::Zero(0);
    m_interface->set_data(newX,newY);
}

void QtRADataManager::save_as()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save GP Data File"),
                                "untitled.dat",
                                tr("Data Files (*.dat)"));
    save_gp_data(fileName.toAscii().data(),m_interface->get_data(),m_interface->get_targets());
}

void QtRADataManager::save()
{
    save_gp_data(ui.path_edit->text().toAscii().data(),m_interface->get_data(),m_interface->get_targets());
}

bool QtRADataManager::open_gp_data(std::string path, Eigen::MatrixXd &data, Eigen::VectorXd &targets)
{
    ifstream in_file;
    in_file.open(path.c_str(),ios_base::in);

    unsigned int rows;
    unsigned int cols;

    in_file >> rows >> cols;

    data.resize(rows,cols);

    for (unsigned int i = 0; i < (unsigned int)data.rows(); i++)
    {
        for (unsigned int j = 0; j < (unsigned int)data.cols(); j++)
        {
            in_file >> data(i,j);
        }
    }

    in_file >> rows >> cols;

    if(cols != 1)
    {
        cout<<"QtRADataManager::open_gp_data Multi dimentional output not supported"<<endl;
        return false;
    }

    targets.resize(rows);

    for (unsigned int i = 0; i < (unsigned int)data.rows(); i++)
    {
        in_file >> targets(i);
    }

    in_file.close();

    return true;
}

bool QtRADataManager::save_gp_data(std::string path, Eigen::MatrixXd const& data, Eigen::VectorXd const& targets)
{
    ofstream out_file;
    out_file.open(path.c_str(),ios_base::out);
    out_file.precision(10);
    out_file << data.rows() << "\t" << data.cols() << endl;
    out_file << data << endl;
    out_file << targets.rows() << "\t" <<targets.cols() << endl;
    out_file << targets << endl;
    out_file.close();
    return true;
}


void QtRADataManager::change_selected_data_point_x(int new_row)
{
    ui.y_list_view->setCurrentRow(new_row);
}

void QtRADataManager::change_selected_data_point_y(int new_row)
{
    ui.x_list_view->setCurrentRow(new_row);
}

void QtRADataManager::fill_data_inspection(Eigen::MatrixXd data,Eigen::VectorXd targets)
{
    ui.x_list_view->clear();
    ui.y_list_view->clear();

    for(unsigned int i = 0; i < (unsigned int)data.rows(); i++)
    {
        QString xAll;
        for(unsigned int j = 0; j < (unsigned int)data.cols(); j++)
        {
            QString x;
            if(j == 0 && data.cols() > 1)
            {
                xAll.append("[");
            }
            x.setNum(data(i,j));
            xAll.append(x);
            if(j == (unsigned int)data.cols() -1 && data.cols() > 1)
            {
                xAll.append("]");
            }
            else if(j != (unsigned int)data.cols() -1 && data.cols() > 1)
            {
                xAll.append(",");
            }
        }
        ui.x_list_view->addItem(xAll);

        QString y;
        y.setNum(targets(i));
        ui.y_list_view->addItem(y);
    }
    if((unsigned int)data.rows() > 0)
    {
        ui.x_list_view->setCurrentRow(0);
        ui.y_list_view->setCurrentRow(0);
    }
}

