#include <apps/regression_app/qt_interface/qt_ra_gp_options.h>
#include <apps/regression_app/qt_interface/qt_regression_app_interface.h>
#include <iostream>
#include <vector>

using namespace std;

QtRAGPOptions::QtRAGPOptions(QWidget *parent, Qt::WFlags flags)
    : QDockWidget(parent,flags)
    , m_interface(NULL)
{
    ui.setupUi(this);

    ui.automatic_inference_check_box->setChecked(true);
    ui.resolution_inference_edit->setEnabled(false);
    ui.min_inference_edit->setEnabled(false);
    ui.max_inference_edit->setEnabled(false);
    create_qt_connections();
}

QtRAGPOptions::~QtRAGPOptions()
{

}

void QtRAGPOptions::update()
{
    QString selectedCovFunc = ui.cov_func_type_combo_box->currentText();
    if (selectedCovFunc == "Squared Exponential")
    {
        change_cov_function_type("Squared Exponential");
    }
    else if (selectedCovFunc == "Neural Network")
    {
        change_cov_function_type("Neural Network");
    }
    ui.noise_edit->setValue(m_interface->get_noise_std());
}

Eigen::MatrixXd QtRAGPOptions::get_inference_domain()
{
    Eigen::MatrixXd test_points;
    double offset;


    switch(m_interface->get_plotting_mode())
    {
        case QtRAInterface::PLOT_2D:
            if(ui.automatic_inference_check_box->isChecked())
            {
                offset = 1;
                if(m_interface->get_data().rows()>1)
                {
                    m_min = m_interface->get_data().array().minCoeff();
                    m_max = m_interface->get_data().array().maxCoeff();
                    if(m_max != m_min)
                    {
                        m_resolution = (m_max-m_min)/100;
                    }
                }
            }
            else
            {
                offset = 0;
                m_resolution = ui.resolution_inference_edit->value();
                m_min = ui.min_inference_edit->value();
                m_max = ui.max_inference_edit->value();
                if(m_max<m_min)
                {
                    m_max = m_min;
                    ui.max_inference_edit->setValue(m_max);
                }
                if(floor((m_max-m_min)/m_resolution) > 1000)
                {
                    m_resolution = (m_max-m_min)/1000;
                    ui.resolution_inference_edit->setValue(m_resolution);
                }
            }
            test_points = Eigen::MatrixXd((unsigned int)floor((m_max-m_min)/m_resolution),1);
            test_points.col(0) = Eigen::ArrayXd::LinSpaced(floor((m_max-m_min)/m_resolution),m_min-offset,m_max+offset);
        break;
        case QtRAInterface::PLOT_3D:
        if(ui.automatic_inference_check_box->isChecked())
        {
            offset = 1;
            if(m_interface->get_data().rows()>1)
            {
                m_min = m_interface->get_data().array().minCoeff();
                m_max = m_interface->get_data().array().maxCoeff();
                if(m_max != m_min)
                {
                    m_resolution = (m_max-m_min)/40;
                }
            }
        }
        else
        {
            offset = 0;
            m_resolution = ui.resolution_inference_edit->value();
            m_min = ui.min_inference_edit->value();
            m_max = ui.max_inference_edit->value();
            if(m_max<m_min)
            {
                m_max = m_min;
                ui.max_inference_edit->setValue(m_max);
            }
            if(floor((m_max-m_min)/m_resolution) > 100)
            {
                m_resolution = (m_max-m_min)/100;
                ui.resolution_inference_edit->setValue(m_resolution);
            }
        }
        Eigen::ArrayXd a = Eigen::ArrayXd::LinSpaced(floor((m_max-m_min)/m_resolution),m_min-offset,m_max+offset);
        Eigen::ArrayXd b = Eigen::ArrayXd::LinSpaced(floor((m_max-m_min)/m_resolution),m_min-offset,m_max+offset);
        test_points.resize(a.size()*b.size(),2);
        for(unsigned int i=0; i < (unsigned int)a.size() ; i++)
        {
            for(unsigned int j = 0; j < (unsigned int)b.size(); j++)
            {
                Eigen::ArrayXd aux(2);
                aux(0) = a(i);
                aux(1) = b(j);
                test_points.row(i*b.size()+j) = aux;
            }
        }
        break;
    }
    return test_points;
}

void QtRAGPOptions::get_regression_domain_params(unsigned int &number_of_divisions_per_dim,double &min,double &max,double &resolution)
{
    number_of_divisions_per_dim =  floor((m_max-m_min)/m_resolution);
    min = m_min;
    max = m_max;
    resolution = m_resolution;
}

void QtRAGPOptions::create_qt_connections()
{
    connect(ui.cov_func_type_combo_box,SIGNAL(currentIndexChanged(QString)),this,SLOT(change_cov_function_type(QString)));
    connect(ui.cov_func_params_list,SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)),this,SLOT(change_selected_hyper_param(QListWidgetItem *,QListWidgetItem *)));
    connect(ui.cov_func_params_modifier,SIGNAL(editingFinished()), this, SLOT(modify_hyperparameter()));
    connect(ui.noise_edit,SIGNAL(editingFinished()),this,SLOT(modify_noise()));
    connect(ui.optimiser_button,SIGNAL(pressed()),this,SLOT(optimise()));
    connect(ui.automatic_inference_check_box,SIGNAL(stateChanged(int)),this,SLOT(inference_mode_changed(int)));
    connect(ui.resolution_inference_edit,SIGNAL(editingFinished()), this, SLOT(inference_domain_changed()));
    connect(ui.min_inference_edit,SIGNAL(editingFinished()), this, SLOT(inference_domain_changed()));
    connect(ui.max_inference_edit,SIGNAL(editingFinished()), this, SLOT(inference_domain_changed()));
}

void QtRAGPOptions::inference_domain_changed()
{
    m_interface->inference();
}

void QtRAGPOptions::inference_mode_changed(int new_mode)
{
    if(new_mode == Qt::Checked)
    {
        ui.resolution_inference_edit->setEnabled(false);
        ui.min_inference_edit->setEnabled(false);
        ui.max_inference_edit->setEnabled(false);
    }
    else
    {
        ui.resolution_inference_edit->setEnabled(true);
        ui.min_inference_edit->setEnabled(true);
        ui.max_inference_edit->setEnabled(true);
    }
    m_interface->inference();
}

void QtRAGPOptions::change_selected_hyper_param(QListWidgetItem *current,QListWidgetItem *previous)
{
    if(current != NULL)
    {
        QString value_string = current->text();
        double value = value_string.toDouble();
        ui.cov_func_params_modifier->setValue(value);
    }
}

void QtRAGPOptions::change_cov_function_type(QString new_type)
{
    std::vector<double> params;
    bool has_derivatives = false;
    params = m_interface->get_cov_func_params();
    has_derivatives = m_interface->has_derivatives();

    fill_params_list(params);

    ui.goal_function_exact_derivative->setEnabled(has_derivatives);
    if(!has_derivatives)
        ui.goal_function_exact_derivative->setCheckState(Qt::Unchecked);
    m_interface->inference();
}

void QtRAGPOptions::fill_params_list(std::vector<double> params)
{
    ui.cov_func_params_list->clear();
    for(unsigned int i = 0; i < params.size(); i++)
    {
        QString param;
        param.setNum(params[i]);
        ui.cov_func_params_list->addItem(param);
    }
    if(params.size()>0)
    {
        ui.cov_func_params_list->setCurrentRow(0);
    }
}

void QtRAGPOptions::modify_hyperparameter()
{
    QString new_value_string;
    double new_value = ui.cov_func_params_modifier->value();
    new_value_string.setNum(new_value);
    ui.cov_func_params_list->currentItem()->setText(new_value_string);
    if(m_interface != NULL && (new_value != m_interface->get_cov_func_params()[ui.cov_func_params_list->currentRow()]))
    {
        m_interface->set_param(new_value,ui.cov_func_params_list->currentRow());
    }
}

void QtRAGPOptions::modify_noise()
{
    double new_noise_value = ui.noise_edit->value();
    if (m_interface != NULL && new_noise_value != m_interface->get_noise_std())
    {
        m_interface->set_noise_std(new_noise_value);
    }
}

void QtRAGPOptions::optimise()
{
    m_interface->set_grad_calculation_internal(!ui.goal_function_exact_derivative->isChecked());
    m_interface->learn();
}

void QtRAGPOptions::set_goal_function_value(double value)
{
    QString value_string;
    value_string.setNum(value);
    if (ui.goal_function_label!=NULL)
        ui.goal_function_label->setText(value_string);
}

unsigned int QtRAGPOptions::get_selected_cov_func()
{
    QString selectedCovFunc = ui.cov_func_type_combo_box->currentText();
    if (selectedCovFunc == "Squared Exponential")
    {
        return 0;
    }
    else if (selectedCovFunc == "Neural Network")
    {
        return 1;
    }
    cout<<"QtRAGPOptions::get_selected_type(): Type not implemented"<<endl;
    exit(-1);
}
