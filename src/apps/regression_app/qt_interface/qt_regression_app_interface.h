#ifndef _Qt_RA_INTERFACE_H_
#define _Qt_RA_INTERFACE_H_

#include <QtGui/QMainWindow>
#include <Eigen/Core>
#include <apps/regression_app/qt_interface/qt_plot_widget.h>
#include <apps/regression_app/qt_interface/qt_3d_plot_widget.h>
#include <apps/regression_app/qt_interface/qt_ra_gp_options.h>
#include "ui_qt_regression_app_interface.h"
#include "../regression_app_generic_interface.h"

class QwtPlotZoomer;
class QwtPlotPicker;
class QwtPlotPanner;
class QMenu;
class QtRADataManager;

class QtRAInterface
    : public QMainWindow
    , public RAGenericInterface
{
    Q_OBJECT

    public:
        enum PlottingMode
        {
            PLOT_2D,
            PLOT_3D
        };

    public:
        QtRAInterface(RegressionAppCore* core, QWidget *parent = 0, Qt::WFlags flags = 0);
        ~QtRAInterface();

        void create_qt_connections();

        void set_initial_gp_params();

        void set_grad_calculation_internal(bool const& policy);
        bool has_derivatives();

        void create_zoomer();

        void set_params(std::vector<double> const& param);
        void set_param(double const& paramValue, int const& index);
        void set_noise_std(double const& noise_std);
        double get_noise_std();
        void learn();

        std::vector<double> get_cov_func_params();

        void add_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        void set_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
                );

        void inference();

        void get_regression_domain_params(unsigned int &number_of_divisions_per_dim,double &min,double &max,double &resolution);

        Eigen::MatrixXd get_data();
        Eigen::VectorXd get_targets();

        void set_plotting_mode(PlottingMode plotting_mode);
        PlottingMode get_plotting_mode();

    signals:
        void update_signal();

    public slots:
        void open_data_manager();
        void update();

    private slots:
    #ifndef QT_NO_PRINTER
        void print();
    #endif
    void exportDocument();
    void enableZoomMode(bool on);
    void add_new_point(QPointF new_point);

    private:
        PlottingMode m_plotting_mode;

        Qt3DPlotWidget *m_plot_3d_mean_widget;
        Qt3DPlotWidget *m_plot_3d_var_widget;

        QtPlotWidget *m_plot_widget;
        QwtPlotZoomer *m_zoomer[2];
        QwtPlotPicker *m_picker;
        QwtPlotPicker *m_data_adder;
        QwtPlotPanner *m_panner;

        void showInfo(QString text = QString::null);
        QtRAGPOptions *m_gp_options_widget;
        QtRADataManager *m_data_manager_widget;

        Ui::QtRAInterface m_ui;

        void create_qt_actions();
        void create_menu();
        QMenu *m_window_menu;
        QAction *m_open_data_manager_action;
};

#endif //_Qt_RA_INTERFACE_H_
