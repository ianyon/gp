#include <apps/regression_app/qt_interface/qt_regression_app_interface.h>
#include <apps/regression_app/core/regression_app_core.h>
#include <apps/regression_app/qt_interface/qt_ra_data_manager.h>
#include "../pixmaps.h"
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qwt_counter.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <QPrinter>
#include <QPrintDialog>
#include <QImageWriter>
#include <QFileDialog>
#include <QMenu>
#include <iostream>

using namespace std;

class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):
        QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Shift+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);
        setMousePattern(QwtEventPattern::MouseSelect3,
                        Qt::RightButton, Qt::ShiftModifier);
    }
};

class Picker: public QwtPlotPicker
{
public:
    Picker(int xAxis, int yAxis, QwtPicker::RubberBand rubberBand, QwtPicker::DisplayMode displayMode, QwtPlotCanvas *canvas)
        :QwtPlotPicker(xAxis,yAxis,rubberBand,displayMode,canvas)
    {

    }
};

QtRAInterface::QtRAInterface(RegressionAppCore *core, QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent,flags)
    , RAGenericInterface(core)
{
    m_zoomer[0] = NULL;
    m_zoomer[1] = NULL;

    m_ui.setupUi(this);

    m_plot_widget = new QtPlotWidget();
    int margin = 5;
    m_plot_widget->setContentsMargins( margin, margin, margin, 0 );

    m_plot_3d_mean_widget = new Qt3DPlotWidget(this,"Gp Mean");
    m_plot_3d_var_widget = new Qt3DPlotWidget(this,"GP Variance");

    m_gp_options_widget = new QtRAGPOptions(this);
    m_gp_options_widget->m_interface = this;
    addDockWidget(Qt::RightDockWidgetArea,m_gp_options_widget);

    m_data_manager_widget = new QtRADataManager(this);
    m_data_manager_widget->m_interface = this;
    m_data_manager_widget->hide();
    setContextMenuPolicy(Qt::NoContextMenu);

    set_initial_gp_params();
    set_plotting_mode(PLOT_2D);
    m_gp_options_widget->change_cov_function_type("Squared Exponential");
    inference();

    create_zoomer();

    m_panner = new QwtPlotPanner(m_plot_widget->canvas());
    m_panner->setMouseButton(Qt::MidButton);

    m_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
        QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
       m_plot_widget->canvas());
    m_picker->setStateMachine(new QwtPickerDragPointMachine());
    m_picker->setRubberBandPen(QPen(QColor(255,0,0,100),2,Qt::DashLine));
    m_picker->setRubberBand(QwtPicker::CrossRubberBand);
    m_picker->setTrackerPen(QColor(Qt::black));

    m_data_adder = new Picker(QwtPlot::xBottom, QwtPlot::yLeft,
                              QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOff,
        m_plot_widget->canvas());
    m_data_adder->setStateMachine(new QwtPickerDragPointMachine());
    m_data_adder->setMousePattern(QwtPicker::MouseSelect1, Qt::RightButton);
    m_data_adder->setRubberBandPen(QPen(QColor(0,255,0,100),2,Qt::DashLine));
    m_data_adder->setRubberBand(QwtPicker::CrossRubberBand);
    m_data_adder->setTrackerPen(QColor(Qt::black));
    connect(m_data_adder,SIGNAL(appended(QPointF)),this,SLOT(add_new_point(QPointF)));

    m_plot_widget->show();

    QWidget *main_widget = new QWidget(this);
    QVBoxLayout *main_layout = new QVBoxLayout(main_widget);
    main_layout->addWidget(m_plot_widget);
    main_widget->setLayout(main_layout);

    setCentralWidget(main_widget);

    resize(800,400);

    QToolBar *toolBar = new QToolBar(this);

    QToolButton *btnZoom = new QToolButton(toolBar);
    btnZoom->setText("Zoom");
    btnZoom->setIcon(QIcon(zoom_xpm));
    btnZoom->setCheckable(true);
    btnZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnZoom);
    connect(btnZoom, SIGNAL(toggled(bool)), SLOT(enableZoomMode(bool)));

#ifndef QT_NO_PRINTER
    QToolButton *btnPrint = new QToolButton(toolBar);
    btnPrint->setText("Print");
    btnPrint->setIcon(QIcon(print_xpm));
    btnPrint->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnPrint);
    connect(btnPrint, SIGNAL(clicked()), SLOT(print()));
#endif

    QToolButton *btnExport = new QToolButton(toolBar);
    btnExport->setText("Export");
    btnExport->setIcon(QIcon(print_xpm));
    btnExport->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnExport);
    connect(btnExport, SIGNAL(clicked()), SLOT(exportDocument()));

    toolBar->addSeparator();

    QWidget *hBox = new QWidget(toolBar);

    QHBoxLayout *layout = new QHBoxLayout(hBox);
    layout->setSpacing(0);
    layout->addWidget(new QWidget(hBox), 10); // spacer
    layout->addSpacing(10);

    (void)toolBar->addWidget(hBox);

    addToolBar(toolBar);

#ifndef QT_NO_STATUSBAR
    (void)statusBar();
#endif

    enableZoomMode(false);
    showInfo();

    create_qt_connections();
    create_qt_actions();
    create_menu();
    open_data_manager();
}

QtRAInterface::~QtRAInterface()
{

}

#ifndef QT_NO_PRINTER

void QtRAInterface::print()
{
    QPrinter printer(QPrinter::HighResolution);

    QString docName = m_plot_widget->title().text();
    if ( !docName.isEmpty() )
    {
        docName.replace (QRegExp (QString::fromLatin1 ("\n")), tr (" -- "));
        printer.setDocName (docName);
    }

    printer.setCreator("GP Regression");
    printer.setOrientation(QPrinter::Landscape);

    QPrintDialog dialog(&printer);
    if ( dialog.exec() )
    {
        QwtPlotRenderer renderer;

        if ( printer.colorMode() == QPrinter::GrayScale )
        {
            renderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasBackground);
            renderer.setLayoutFlag(QwtPlotRenderer::FrameWithScales);
        }

        renderer.renderTo(m_plot_widget, printer);
    }
}

#endif

void QtRAInterface::exportDocument()
{
#ifndef QT_NO_PRINTER
    QString fileName = "gpRegression.pdf";
#else
    QString fileName = "gpRegression.png";
#endif

#ifndef QT_NO_FILEDIALOG
    const QList<QByteArray> imageFormats =
        QImageWriter::supportedImageFormats();

    QStringList filter;
    filter += "PDF Documents (*.pdf)";
#ifndef QWT_NO_SVG
    filter += "SVG Documents (*.svg)";
#endif
    filter += "Postscript Documents (*.ps)";

    if ( imageFormats.size() > 0 )
    {
        QString imageFilter("Images (");
        for ( int i = 0; i < imageFormats.size(); i++ )
        {
            if ( i > 0 )
                imageFilter += " ";
            imageFilter += "*.";
            imageFilter += imageFormats[i];
        }
        imageFilter += ")";

        filter += imageFilter;
    }

    fileName = QFileDialog::getSaveFileName(
        this, "Export File Name", fileName,
        filter.join(";;"), NULL, QFileDialog::DontConfirmOverwrite);
#endif

    if ( !fileName.isEmpty() )
    {
        QwtPlotRenderer renderer;

        // flags to make the document look like the widget
        renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, false);
        //renderer.setLayoutFlag(QwtPlotRenderer::DiscardNone, true);

        renderer.renderDocument(m_plot_widget, fileName, QSizeF(300, 200), 85);
    }
}

void QtRAInterface::set_initial_gp_params()
{
    //initial values of the hyper parameters
    std::vector<double> hyper_params;
    hyper_params.push_back(1);
    hyper_params.push_back(2);
    m_core->set_params(hyper_params);

    //initial test data
    Eigen::MatrixXd X(20,1);
    Eigen::ArrayXd y(20);
    X <<-2.1775,-0.9235,0.7502,-5.8868,-2.7995,4.2504,2.4582,6.1426,-4.0911,-6.3481,1.0004,-4.7591,0.4715,4.8933,4.3248,-3.7461,-7.3005,5.8177,2.3851,-6.3772;
    y <<1.4121,1.6936,-0.7444,0.2493,0.3978,-1.2755,-2.221,-0.8452,-1.2232,0.0105,-1.0258,-0.8207,-0.1462,-1.5637,-1.098,-1.1721,-1.7554,-1.0712,-2.6937,-0.0329;
    m_core->set_data(X,y);

    m_core->set_noise_std(0.1);
}

void QtRAInterface::create_zoomer()
{
    if(m_zoomer[0] != NULL)
    {
        delete m_zoomer[0];
    }
    if(m_zoomer[1] != NULL)
    {
        delete m_zoomer[1];
    }
    m_zoomer[0] = new Zoomer( QwtPlot::xBottom, QwtPlot::yLeft,
                                m_plot_widget->canvas());
    m_zoomer[0]->setRubberBand(QwtPicker::RectRubberBand);
    m_zoomer[0]->setRubberBandPen(QPen(QColor(255,0,0,100),2,Qt::DashLine));
    m_zoomer[0]->setTrackerMode(QwtPicker::ActiveOnly);
    m_zoomer[0]->setTrackerPen(QColor(Qt::black));

    m_zoomer[1] = new Zoomer(QwtPlot::xTop, QwtPlot::yRight,
                                m_plot_widget->canvas());
}

void QtRAInterface::add_new_point(QPointF new_point)
{
    const QPointF p = new_point;

    Eigen::MatrixXd newX(1,1);
    newX(0,0) = p.x();

    Eigen::VectorXd newY(1);
    newY(0) = p.y();

    add_data(newX,newY);
}

void QtRAInterface::add_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
)
{
    if(m_core->get_data().rows()>0)
    {
        m_core->add_data(data,labels);
        inference();
    }
    else
    {
        set_data(data,labels);
    }
}

void QtRAInterface::enableZoomMode(bool on)
{
    m_panner->setEnabled(on);

    m_zoomer[0]->setEnabled(on);
    m_zoomer[0]->zoom(0);

    m_zoomer[1]->setEnabled(on);
    m_zoomer[1]->zoom(0);

    showInfo();
}

void QtRAInterface::create_qt_connections()
{

}

void QtRAInterface::create_qt_actions()
{
    m_open_data_manager_action = new QAction(tr("Data Manager"), this);
    m_open_data_manager_action->setShortcut(tr("Ctrl+D"));
    m_open_data_manager_action->setStatusTip(tr("Open/hide data manager."));
    m_open_data_manager_action->setCheckable(true);
    connect(m_open_data_manager_action, SIGNAL(triggered()), this, SLOT(open_data_manager()));
}

void QtRAInterface::create_menu()
{
    m_window_menu = menuBar()->addMenu(tr("&Window"));
    m_window_menu->addAction(m_open_data_manager_action);
}

void QtRAInterface::open_data_manager()
{
    if(m_data_manager_widget->isVisible())
    {
        m_data_manager_widget->hide();
    }
    else
    {
        m_data_manager_widget->show();
        addDockWidget(Qt::LeftDockWidgetArea,m_data_manager_widget);

    }
        m_data_manager_widget->update();
}

void QtRAInterface::set_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
        )
{
    m_core->clear_training_data();
    if(data.cols() == 1 && get_plotting_mode() != PLOT_2D)
    {
        set_plotting_mode(PLOT_2D);

        cout<<"plot2d"<<endl;
    }
    else if (data.cols() == 2 && get_plotting_mode() != PLOT_3D)
    {
        set_plotting_mode(PLOT_3D);

        cout<<"plot3d"<<endl;
    }
    m_core->set_data(data,labels);
    m_gp_options_widget->update();
}

void QtRAInterface::set_plotting_mode(PlottingMode plotting_mode)
{
    m_plotting_mode = plotting_mode;
    std::vector<double> hyper_params;
    switch(m_plotting_mode)
    {
        case PLOT_2D:
            m_plot_widget = new QtPlotWidget();
            //initial values of the hyper parameters
            hyper_params.push_back(1);
            hyper_params.push_back(2);
            m_core->set_params(hyper_params);

            m_core->set_noise_std(0.1);
            break;
        case PLOT_3D:
            m_plot_3d_mean_widget = new Qt3DPlotWidget(this,"GP Mean");
            m_plot_3d_var_widget = new Qt3DPlotWidget(this,"GP Variance");
            hyper_params.push_back(1);
            hyper_params.push_back(2);
            hyper_params.push_back(2);
            m_core->set_params(hyper_params);

            break;
    }
    if (m_gp_options_widget != NULL)
    {

    }

}

QtRAInterface::PlottingMode QtRAInterface::get_plotting_mode()
{
    return m_plotting_mode;
}

void QtRAInterface::update()
{
    switch (m_plotting_mode)
    {
        case PLOT_2D:
            if(m_plot_widget != NULL)
            {
                QWidget *main_widget = new QWidget(this);
                QGridLayout *main_layout = new QGridLayout(main_widget);
                main_layout->addWidget(m_plot_widget);
                main_widget->setLayout(main_layout);
                setCentralWidget(main_widget);
                m_plot_widget->update();
                resize(800,400);
            }
            break;
        case PLOT_3D:
            if(m_plot_3d_mean_widget != NULL && m_plot_3d_var_widget != NULL)
            {
                QWidget *main_widget = new QWidget(this);
                QGridLayout *main_layout = new QGridLayout(main_widget);
                main_layout->addWidget(m_plot_3d_mean_widget);
                main_layout->addWidget(m_plot_3d_var_widget);
                main_widget->setLayout(main_layout);
                setCentralWidget(main_widget);

            }
            break;
    }

    if(m_data_manager_widget != NULL)
    {
        m_data_manager_widget->update();
    }

}

void QtRAInterface::inference()
{
    GPResult output;
    Eigen::MatrixXd domain = m_gp_options_widget->get_inference_domain();
    if(get_plotting_mode() == PLOT_2D)
    {
        output = m_core->inference(domain);
        m_gp_options_widget->set_goal_function_value(output.goal_function);
        m_plot_widget->set_regression_data(domain,output.mean, output.variance);
        m_plot_widget->set_sample_data(m_core->get_data(),m_core->get_labels());
    }
    else if(get_plotting_mode() == PLOT_3D)
    {
        output = m_core->inference(domain);
        m_plot_3d_mean_widget->set_regression_data(output.mean);
        m_plot_3d_var_widget->set_regression_data(output.variance);
    }
    update();
}

void QtRAInterface::get_regression_domain_params(unsigned int &number_of_divisions_per_dim,double &min,double &max,double &resolution)
{
    m_gp_options_widget->get_regression_domain_params(number_of_divisions_per_dim,min,max,resolution);
}

Eigen::MatrixXd QtRAInterface::get_data()
{
    return m_core->get_data();
}

Eigen::VectorXd QtRAInterface::get_targets()
{
    return m_core->get_labels();
}

std::vector<double> QtRAInterface::get_cov_func_params()
{
    return m_core->get_params();
}

void QtRAInterface::set_params(std::vector<double> const& params)
{
    m_core->set_params(params);
    inference();
}

void QtRAInterface::set_param(double const& paramValue, int const& index)
{
    std::vector<double> current_params = m_core->get_params();
    current_params[index] = paramValue;
    set_params(current_params);
}

void QtRAInterface::set_noise_std(double const& noise_std)
{
    m_core->set_noise_std(noise_std);
    inference();
}

double QtRAInterface::get_noise_std()
{
    return m_core->get_noise_std();
}

void QtRAInterface::set_grad_calculation_internal(bool const& policy)
{
    m_core->set_grad_calculation_internal(policy);
}

bool QtRAInterface::has_derivatives()
{
    return m_core->has_derivatives();
}

void QtRAInterface::learn()
{
    m_core->learn_params();
    if (m_gp_options_widget != NULL)
        m_gp_options_widget->update();
    inference();
}

void QtRAInterface::showInfo(QString text)
{
    if ( text == QString::null )
    {
        if ( m_picker->rubberBand() )
            text = "Press right mouse button to add data.";
        else
            text = "Zoom: Press mouse button and drag.";
    }

#ifndef QT_NO_STATUSBAR
    statusBar()->showMessage(text);
#endif
}
