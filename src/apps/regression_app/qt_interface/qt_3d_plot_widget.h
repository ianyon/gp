#ifndef _QT_3D_PLOT_WIDGET_
#define _QT_3D_PLOT_WIDGET_

#include <qwt3d_surfaceplot.h>
#include <qwt3d_function.h>

#include <Eigen/Core>

class QtRAInterface;

class Qt3DPlotWidget : public Qwt3D::SurfacePlot
{
public:
    Qt3DPlotWidget(QtRAInterface *interface, QString name);
    ~Qt3DPlotWidget();

    void set_sample_data(
            Eigen::MatrixXd const&  data,
            Eigen::VectorXd const&  labels
            );

    void set_regression_data(Eigen::ArrayXd const& mean);

    QtRAInterface *m_interface;

    double m_x_max;
    double m_x_min;
    double m_y_max;
    double m_y_min;
    double m_z_min;
    double m_resolution;

    QString m_name;
//protected:
//    virtual void paintEvent(QPaintEvent *);

};

#endif //_QT_3D_PLOT_WIDGET_
