project(regression_app)

find_package( Qt4 REQUIRED )
find_package( Qwt REQUIRED )
find_package( Qwt3D REQUIRED )
find_package( OpenGL REQUIRED )

if( QT4_FOUND AND QWT_FOUND AND QWT_3D_FOUND AND OPENGL_FOUND)

    include_directories(${QT_INCLUDE_DIR})
    include_directories(${QWT_INCLUDE_DIRS})
    include_directories(${QWT_3D_INCLUDE_DIRS})

    set(regression_app_SOURCES
        main.cpp
        qt_interface/qt_regression_app_interface.cpp
        qt_interface/qt_plot_widget.cpp
        qt_interface/qt_3d_plot_widget.cpp
        qt_interface/qt_ra_gp_options.cpp
        qt_interface/qt_ra_data_manager.cpp
        core/regression_app_core.cpp
    )

    set(regression_app_MOC_HEADERS
        qt_interface/qt_regression_app_interface.h
        qt_interface/qt_ra_gp_options.h
        qt_interface/qt_ra_data_manager.h
    )

    set(regression_app_FORMS
        qt_interface/qt_ra_gp_options.ui
        qt_interface/qt_regression_app_interface.ui
        qt_interface/qt_ra_data_manager.ui
    )

    #set(regression_app_RESOURCES images.qrc)

    qt4_wrap_cpp(regression_app_MOC_HEADERS ${regression_app_MOC_HEADERS})
    qt4_wrap_ui(regression_app_FORMS_HEADERS ${regression_app_FORMS})

    include(${QT_USE_FILE})
    add_definitions(${QT_DEFINITIONS})

    add_executable(regression_app
        ${regression_app_SOURCES}
        ${regression_app_MOC_HEADERS}
        ${regression_app_FORMS_HEADERS}
    )
    target_link_libraries(regression_app
        ${LIB_GP}
        ${LIB_GOALFUNC}
        ${LIB_COVFUNC}
        ${LIB_MEANFUNC}
        ${LIB_OPTIMISER}
        ${NLOPT_LIBRARIES}
        dl
        ${QT_LIBRARIES}
        ${QWT_LIBRARIES}
        ${QWT_3D_LIBRARIES}
#        GL
        GLU
        "/usr/lib/x86_64-linux-gnu/libQtOpenGL.so.4"
    )

    include_directories(${CMAKE_CURRENT_BINARY_DIR})

endif()

