#ifndef _RA_GENERIC_INTERFACE_H_
#define _RA_GENERIC_INTERFACE_H_

class RegressionAppCore;

class RAGenericInterface
{
    public:
        RAGenericInterface(RegressionAppCore *core):m_core(core){};

        virtual ~RAGenericInterface(void){};

        RegressionAppCore *m_core;
};

#endif
