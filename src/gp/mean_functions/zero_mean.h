#ifndef __GP_ZERO_MEAN_H__
#define __GP_ZERO_MEAN_H__


#include <Eigen/Core>
#include <gp/mean_functions/abstract_mean_function.h>

namespace gp
{

class ZeroMean
    :public AbstractMeanFunction
{
    public:
        Eigen::VectorXd do_mean(Eigen::MatrixXd const& data);
};
    
} /* gp */

#endif /* __GP_ZERO_MEAN_H__ */
