#include <gp/mean_functions/zero_mean.h>

namespace gp
{

Eigen::VectorXd ZeroMean::do_mean(Eigen::MatrixXd const& data)
{
    return Eigen::VectorXd::Zero(data.rows());
}

} /* gp */
