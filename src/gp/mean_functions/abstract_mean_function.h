#ifndef __GP_ABSTRACT_MEAN_FUNCTION_H__
#define __GP_ABSTRACT_MEAN_FUNCTION_H__


#include <Eigen/Core>

namespace gp
{

class AbstractMeanFunction
{

    public:
        AbstractMeanFunction(){}
        virtual ~AbstractMeanFunction();

    private:
        virtual Eigen::VectorXd do_mean(Eigen::MatrixXd const& data) = 0;

    public:
        Eigen::VectorXd mean(Eigen::MatrixXd const& data);
};
    
} /* gp */

#endif /* __GP_ABSTRACT_MEAN_FUNCTION_H__ */
