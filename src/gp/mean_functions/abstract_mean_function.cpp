#include <gp/mean_functions/abstract_mean_function.h>

namespace gp
{

AbstractMeanFunction::~AbstractMeanFunction()
{

}

Eigen::VectorXd AbstractMeanFunction::mean(Eigen::MatrixXd const& data)
{
    return do_mean(data);
}

} /* gp */
