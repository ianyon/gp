#ifndef __GP_GAUSSIAN_PROCESS_NN_H__
#define __GP_GAUSSIAN_PROCESS_NN_H__

#include <gp/gp/gaussian_process.h>

namespace gp
{

/**
 * \brief Gaussian Process Nearest Neighbors. Inherits from a normal GP, but
 *        implements an efficient way of calculating the prediction for a given
 *        point.
 *
 * The covariance matrix is not calculated fully to allow efficient inversion
 * when predicting for new points. This matrix cov_train_train matrix
 * cannot be stored as it is done in GaussianProcess, because it depends on the
 * query points.
 *
 * The procedure when a new set of query points is received is the following:
 * 1. For every query points: Find its k nearest neighbors (k is determined as
 *    a parameter of GaussianProcessNN).
 * 2. Find a prediction of mean and variance for each query point
 */

class GaussianProcessNN
{
    public:

        GaussianProcessNN();

        GaussianProcessNN(boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                        boost::shared_ptr<AbstractMeanFunction> mean_func);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_free_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the query points.
         *
         * The covariance between the query points can be provided by the user
         * if it is available.
         *
         * \param query_points the points for which to make predictions
         * \param covariance covariance matrix of the query data
         */
        void set_query_points(
                Eigen::MatrixXd const&  query_points,
                Eigen::MatrixXd const&  covariance=Eigen::MatrixXd(0,0)
        );

        /**
         * \brief Sets the data and labels of the GP to the given values.
         *
         * Each row of the data matrix is assumed to contain a single
         * example  with the same row of the label vector containing the
         * associated label.
         *
         * \param data the new training data to use
         * \param labels the labels corresponding to the data entries
         */
        void set_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Sets the labels of the training data.
         *
         * This method only changes the labels without changing the training
         * data itself.
         *
         * \param labels new labels for the training data
         */
        void set_training_labels(Eigen::VectorXd const& labels);

        /**
         * \brief Appends to given data to the existing data.
         *
         * \param data the data entries to add
         * \param labels the labels of the entries to be added
         */
        void append_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Removes the last data point.
         */
        void remove_last_training_point();

        /**
         * \brief Sets the noise std of the data.
         *
         * It is assumed that the target values are noisy, where the noise
         * comes from N(0, s), a Gaussian with zero mean and sigma. The sigma
         * paramerter is set using this method.
         *
         * \param noise the new noise value, interpreted as standard deviation
         */
        void set_noise(double const& noise);

        /**
         * \brief Gets the value of the noise std stored for the gp.
         *
         * \return the stored noise value, interpreted as the standard deviation
         */
        double get_noise();

        /**
         * \breif Clears all stored training data.
         */
        void clear_training_data();

        /**
         * \brief Clears all current test points.
         */
        void clear_query_points();

        Eigen::MatrixXd const& get_training_data() const
        {
            return m_training;
        }
        Eigen::MatrixXd & get_training_data()
        {
            return m_training;
        }
        Eigen::VectorXd const& get_training_labels() const
        {
            return m_labels;
        }
        Eigen::VectorXd & get_training_labels()
        {
            return m_labels;
        }

        Eigen::MatrixXd const& get_query_points() const
        {
            return m_query;
        }
        Eigen::MatrixXd & get_query_points()
        {
            return m_query;
        }

        /**
         * \brief Returns the covariance between training and training data.
         *
         * \return covariance between training and training data
         */
        Eigen::MatrixXd const& get_cov_train_train() const
        {
            return m_cov_train_train;
        }
        Eigen::MatrixXd & get_cov_train_train()
        {
            return m_cov_train_train;
        }

        /**
         * \brief Returns the covariance between query and training data.
         * 
         * \return covariance between query and training data
         */
        Eigen::MatrixXd const& get_cov_query_train() const
        {
            return m_cov_query_train;
        }
        Eigen::MatrixXd & get_cov_query_train()
        {
            return m_cov_query_train;
        }

        /**
         * \brief Returns the covariance between querey and query data.
         *
         * \return covariance between query and query data
         */
        Eigen::MatrixXd const& get_cov_query_query() const
        {
            return m_cov_query_query;
        }
        Eigen::MatrixXd & get_cov_query_query()
        {
            return m_cov_query_query;
        }

        /**
         * \brief Returns the Cholesky factorization of the training data covariance.
         *
         * \return Cholesky factorization of the training data covariance matrix
         */
        Eigen::MatrixXd const& get_cholesky_train_train() const
        {
            return m_cholesky_train_train;
        }
        Eigen::MatrixXd & get_cholesky_train_train()
        {
            return m_cholesky_train_train;
        }

    private:
        /**
         * \brief Recomputes the covariance matrix of the training data.
         */
        void recalculate_cov_train_train();
        /**
         * \brief Recomputes the covariance matrix between query and training data.
         */
        void recalculate_cov_query_train();
        /**
         * \brief Recomputes the covariance matrix of the query data.
         */
        void recalculate_cov_query_query();

    private:
        boost::shared_ptr<AbstractCovarianceFunction> m_cov_func;

        boost::shared_ptr<AbstractMeanFunction> m_mean_func;

        //! Training data
        Eigen::MatrixXd                 m_training;
        //! Labels for the training data
        Eigen::VectorXd                 m_labels;
        //! Test points
        Eigen::MatrixXd                 m_query;
        //! Gaussian distributed noise standard deviation.
        // TODO: is noise a 1d parameter?
        double                          m_noise_std;
        //! Covariance matrix for the train-train part of the covariance matrix
        Eigen::MatrixXd                 m_cov_train_train;
        //! Covariance matrix for the query-train part of the covariance matrix
        Eigen::MatrixXd                 m_cov_query_train;
        //! Covariance matrix for the query-query part of the covariance matrix
        Eigen::MatrixXd                 m_cov_query_query;
        //! Cholesky factorization of K matrix
        Eigen::MatrixXd                 m_cholesky_train_train;
};

} /* gp */

#endif /* __GP_GAUSSIAN_PROCESS_NN_H__ */
