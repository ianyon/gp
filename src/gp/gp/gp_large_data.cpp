//#include <gp/gp/gaussian_process_nn.h>

//namespace gp
//{

//class {
//public:
//    template<typename T>
//    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
//} nullPtr;


//GaussianProcessNN::GaussianProcessNN()
//    :m_cov_func(nullPtr)
//    ,m_mean_func(nullPtr)
//{

//}

//GaussianProcessNN::GaussianProcessNN(
//        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
//        boost::shared_ptr<AbstractMeanFunction> mean_func
//)
//    :m_cov_func(cov_func)
//    ,m_mean_func(mean_func)
//{

//}

//void GaussianProcessNN::set_training_data(
//        Eigen::MatrixXd const&          data,
//        Eigen::VectorXd const&          labels
//)
//{
//    if(data.rows() != labels.rows())
//    {
//        throw DimensionError("Row count of data and labels does not match");
//    }
//    m_training = data;
//    m_labels = labels;

//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        recalculate_cov_train_train();

//        if(m_query.rows() > 0 && m_query.cols() == m_training.cols())
//        {
//            recalculate_cov_query_train();
//        }
//    }
//}

//void GaussianProcessNN::clear_training_data()
//{
//    m_training = Eigen::MatrixXd();
//    m_labels = Eigen::VectorXd();
//    m_cov_train_train = Eigen::MatrixXd();
//    m_cholesky_train_train = Eigen::MatrixXd();
//    m_cov_query_train = Eigen::MatrixXd();
//}


//void GaussianProcessNN::append_training_data(
//        Eigen::MatrixXd const&          data,
//        Eigen::VectorXd const&          labels
//)
//{
//    if(data.rows() != labels.rows())
//    {
//        throw DimensionError("Row count of new data does not match");
//    }
//    if(m_training.rows() > 0 && data.cols() != m_training.cols())
//    {
//        throw DimensionError("Dimensionality of old and new data does not match");
//    }

//    int new_rowcount = m_training.rows() + data.rows();

//    // Resize data storage
//    if(m_training.cols() == 0)
//    {
//        m_training.conservativeResize(data.rows(), data.cols());
//    }
//    else
//    {
//        m_training.conservativeResize(new_rowcount, m_training.cols());
//    }
//    m_labels.conservativeResize(new_rowcount);

//    // Append new data at the end
//    m_training.bottomRows(data.rows()) = data;
//    m_labels.tail(labels.rows()) = labels;

//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        // Update query to training data covariance matrix
//        if(m_query.rows() > 0)
//        {
//            m_cov_query_train.conservativeResize(
//                    m_cov_query_train.rows() + data.rows(),
//                    m_query.rows()
//            );
//            m_cov_query_train.bottomRows(data.rows()) =
//                    m_cov_func->cov(data, m_query);
//        }

//        // Update the Cholesky factorization
//        int N = data.rows();
//        int size = m_cholesky_train_train.rows();
//        m_cholesky_train_train.conservativeResize(size+N, size+N);
//        m_cholesky_train_train.rightCols(N) = m_cov_func->cov(m_training, data);
//        m_cholesky_train_train.bottomRightCorner(N, N).noalias() +=
//            m_noise_std*m_noise_std*Eigen::MatrixXd::Identity(N, N);
//        m_cholesky_train_train
//            .topLeftCorner(size, size)
//            .triangularView<Eigen::Upper>()
//            .transpose()
//            .solveInPlace(m_cholesky_train_train.topRightCorner(size,N));
//        m_cholesky_train_train.bottomRightCorner(N, N) = (
//                m_cholesky_train_train.bottomRightCorner(N,N) -
//                m_cholesky_train_train.topRightCorner(size,N).transpose() *
//                m_cholesky_train_train.topRightCorner(size,N)).llt().matrixU();
//    }
//}


//void GaussianProcessNN::remove_last_training_point()
//{
//    // Check if there is something to remove
//    if(m_training.rows() == 0 || m_labels.rows() == 0)
//    {
//        throw DimensionError("There is no old data to remove");
//    }

//    // Remove data and label
//    m_training.conservativeResize(m_training.rows()-1, m_training.cols());
//    m_labels.conservativeResize(m_labels.rows()-1);

//    // Downdate covariance between query and training data
//    m_cov_query_train.conservativeResize(
//            m_cov_query_train.rows()-1, m_query.rows()
//    );

//    // Downdate the Cholesky factorization
//    m_cholesky_train_train.conservativeResize(
//            m_cholesky_train_train.rows()-1,
//            m_cholesky_train_train.cols()-1
//    );
//}


//void GaussianProcessNN::set_noise(
//        double const&  noise_std
//)
//{
//    m_noise_std = noise_std;
//    recalculate_cov_train_train();
//}


//double GaussianProcessNN::get_noise()
//{
//    return m_noise_std;
//}


//void GaussianProcessNN::set_query_points(
//        Eigen::MatrixXd const&          query_points,
//        Eigen::MatrixXd const&          covariance
//)
//{
//    m_query = query_points;
//    if(covariance.cols() == covariance.rows() &&
//       covariance.cols() == query_points.rows()
//    )
//    {
//        m_cov_query_query = covariance;
//    }
//    else
//    {
//        if(query_points.cols() == m_training.cols())
//        {
//           recalculate_cov_query_train();
//        }
//        recalculate_cov_query_query();
//    }
//}


//void GaussianProcessNN::clear_query_points()
//{
//    m_query = Eigen::MatrixXd();
//    m_cov_query_train = Eigen::MatrixXd();
//    m_cov_query_query = Eigen::MatrixXd();
//}

//boost::shared_ptr<AbstractCovarianceFunction> GaussianProcessNN::get_cov_func()
//{
//    return m_cov_func;
//}

//boost::shared_ptr<AbstractMeanFunction> GaussianProcessNN::get_mean_func()
//{
//    return m_mean_func;
//}

//void GaussianProcessNN::set_cov_func(
//        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
//{
//    m_cov_func = cov_func;
//}

//void GaussianProcessNN::set_mean_func(
//        boost::shared_ptr<AbstractMeanFunction> mean_func)
//{
//    m_mean_func = mean_func;
//}


//void GaussianProcessNN::set_hyper_params(
//        AbstractCovarianceFunction::ParamVec_t const& params
//)
//{
//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        m_cov_func->set_hyper_params(params);
//        recalculate_cov_train_train();
//        recalculate_cov_query_train();
//        recalculate_cov_query_query();
//    }
//}


//void GaussianProcessNN::set_free_hyper_params(
//        AbstractCovarianceFunction::ParamVec_t const& params
//)
//{
//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        m_cov_func->set_free_hyper_params(params);
//        recalculate_cov_train_train();
//        recalculate_cov_query_train();
//        recalculate_cov_query_query();
//    }
//}


//void GaussianProcessNN::recalculate_cov_train_train()
//{
//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        if(m_training.rows() > 0 && m_training.cols() > 0)
//        {
//            m_cov_train_train =
//                m_cov_func->cov(m_training) +
//                m_noise_std * m_noise_std *
//                Eigen::MatrixXd::Identity(m_training.rows(), m_training.rows());
//            m_cholesky_train_train =
//                m_cov_train_train.selfadjointView<Eigen::Upper>().llt().matrixU();
//        }
//        else
//        {
//            clear_training_data();
//        }
//    }
//}


//void GaussianProcessNN::recalculate_cov_query_train()
//{
//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        if(m_training.rows() > 0 && m_query.rows() > 0)
//        {
//            m_cov_query_train = m_cov_func->cov(m_training, m_query);
//        }
//    }
//}


//void GaussianProcessNN::recalculate_cov_query_query()
//{
//    //Check if cov_func exists
//    if(m_cov_func)
//    {
//        if(m_query.rows() > 0)
//        {
//            m_cov_query_query = Eigen::VectorXd(m_query.rows());

//            for(int i=0; i<m_query.rows(); ++i)
//            {
//                m_cov_query_query(i) = m_cov_func->cov(m_query.row(i)).value();
//            }
//        }
//    }
//}


//void GaussianProcessNN::set_training_labels(Eigen::VectorXd const& labels)
//{
//    if(labels.rows() != m_training.rows())
//    {
//        throw DimensionError("Dimension mismatch between training data "
//                             "and training labels.");
//    }
//    m_labels = labels;
//}

//} /* gp */
