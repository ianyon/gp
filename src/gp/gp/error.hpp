#ifndef __GP_ERROR_HPP__
#define __GP_ERROR_HPP__


#include <string>


namespace gp
{
    
/**
 * \brief Base class of the exception hierarchy.
 */
class BaseError
{
    public:
        /**
         * \brief Creates a new BaseError instance.
         *
         * \param err the error message
         */
        BaseError(std::string const& err)
            :   m_error(err)
        {}

        /**
         * \brief Returns the stored error string.
         *
         * \return the error string
         */
        std::string error() const
        {
            return m_error;
        }

    private:
        //! Error string of this exception
        std::string                     m_error;
};

/**
 * \brief Error thrown in relation with the dimension of data.
 */
class DimensionError : public BaseError
{
    public:
        /**
         * \brief Creates a new DimensionError instance.
         *
         * \param err the error message
         */
        DimensionError(std::string const& err)
            :   BaseError(err)
        {}
};

/**
 * \brief Error thrown in relation with parameter usage.
 */
class ParameterError : public BaseError
{
    public:
        /**
         * \brief Creates a new ParameterError instance.
         *
         * \param err the error message
         */
        ParameterError(std::string const& err)
            :   BaseError(err)
        {}
};

/**
 * \brief Error thrown in relation with the optimiser usage.
 */
class OptimiserError : public BaseError
{
    public:
        /**
         * \brief Creates a new OptimiserError instance.
         *
         * \param err the error message
         */
        OptimiserError(std::string const& err)
            :   BaseError(err)
        {}
};

/**
 * \brief Error thrown in relation with parameter usage.
 */
class GoalFunctionError : public BaseError
{
    public:
        /**
         * \brief Creates a new GoalFunctionError instance.
         *
         * \param err the error message
         */
        GoalFunctionError(std::string const& err)
            :   BaseError(err)
        {}
};

/**
 * \brief Error thrown in relation with a derivative requested usage.
 */
class DerivativeError : public BaseError
{
    public:
        /**
         * \brief Creates a new ParameterError instance.
         *
         * \param err the error message
         */
        DerivativeError(std::string const& err)
            :   BaseError(err)
        {}
};

} /* gp */


#endif /* __GP_ERROR_HPP__ */
