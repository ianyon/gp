#include <gp/gp/gp_simple.h>

namespace gp
{

class {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
} nullPtr;

GaussianProcess::GaussianProcess()
    :AbstractGaussianProcess(nullPtr,nullPtr)
{

}

GaussianProcess::GaussianProcess(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func
)
    :AbstractGaussianProcess(cov_func,mean_func)
{

}

void GaussianProcess::set_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    //Only do something is parameters have changed.
    if(get_hyper_params() != params)
    {
        AbstractGaussianProcess::set_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_cov_train_train();
        recalculate_cov_query_train();
        recalculate_cov_query_query();
    }
}


void GaussianProcess::set_free_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    if(get_free_hyper_params() != params)
    {
        AbstractGaussianProcess::set_free_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_cov_train_train();
        recalculate_cov_query_train();
        recalculate_cov_query_query();
    }

}

void GaussianProcess::set_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionError("Row count of data and labels does not match");
    }
    m_training = data;
    m_labels = labels;

    //Check if cov_func exists
    if(get_cov_func())
    {
        recalculate_cov_train_train();

        if(get_query_points().rows() > 0 && get_query_points().cols() == m_training.cols())
        {
            recalculate_cov_query_train();
        }
    }
}

void GaussianProcess::clear_training_data()
{
    m_training = Eigen::MatrixXd();
    m_labels = Eigen::VectorXd();
    m_cov_train_train = Eigen::MatrixXd();
    m_cholesky_train_train = Eigen::MatrixXd();
    m_cov_query_train = Eigen::MatrixXd();
}


void GaussianProcess::append_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionError("Row count of new data does not match");
    }
    if(m_training.rows() > 0 && data.cols() != m_training.cols())
    {
        throw DimensionError("Dimensionality of old and new data does not match");
    }

    int new_rowcount = m_training.rows() + data.rows();

    // Resize data storage
    if(m_training.cols() == 0)
    {
        m_training.conservativeResize(data.rows(), data.cols());
    }
    else
    {
        m_training.conservativeResize(new_rowcount, m_training.cols());
    }
    m_labels.conservativeResize(new_rowcount);

    // Append new data at the end
    m_training.bottomRows(data.rows()) = data;
    m_labels.tail(labels.rows()) = labels;

    //Check if cov_func exists
    if(get_cov_func())
    {
        // Update query to training data covariance matrix
        if(get_query_points().rows() > 0)
        {
            m_cov_query_train.conservativeResize(
                    m_cov_query_train.rows() + data.rows(),
                    get_query_points().rows()
            );
            m_cov_query_train.bottomRows(data.rows()) =
                    get_cov_func()->cov(data, get_query_points());
        }

        // Update the Cholesky factorization
        int N = data.rows();
        int size = m_cholesky_train_train.rows();
        m_cholesky_train_train.conservativeResize(size+N, size+N);
        m_cholesky_train_train.rightCols(N) = get_cov_func()->cov(m_training, data);
        m_cholesky_train_train.bottomRightCorner(N, N).noalias() +=
            get_noise()*get_noise()*Eigen::MatrixXd::Identity(N, N);
        m_cholesky_train_train
            .topLeftCorner(size, size)
            .triangularView<Eigen::Upper>()
            .transpose()
            .solveInPlace(m_cholesky_train_train.topRightCorner(size,N));
        m_cholesky_train_train.bottomRightCorner(N, N) = (
                m_cholesky_train_train.bottomRightCorner(N,N) -
                m_cholesky_train_train.topRightCorner(size,N).transpose() *
                m_cholesky_train_train.topRightCorner(size,N)).llt().matrixU();
    }
}


void GaussianProcess::remove_last_training_point()
{
    // Check if there is something to remove
    if(m_training.rows() == 0 || m_labels.rows() == 0)
    {
        throw DimensionError("There is no old data to remove");
    }

    // Remove data and label
    m_training.conservativeResize(m_training.rows()-1, m_training.cols());
    m_labels.conservativeResize(m_labels.rows()-1);

    // Downdate covariance between query and training data
    m_cov_query_train.conservativeResize(
            m_cov_query_train.rows()-1, m_query.rows()
    );

    // Downdate the Cholesky factorization
    m_cholesky_train_train.conservativeResize(
            m_cholesky_train_train.rows()-1,
            m_cholesky_train_train.cols()-1
    );
}


void GaussianProcess::set_noise(
        double const&  noise_std
)
{
    AbstractGaussianProcess::set_noise(noise_std);
    recalculate_cov_train_train();
}

void GaussianProcess::clear_query_points()
{
    AbstractGaussianProcess::clear_query_points();
    m_cov_query_train = Eigen::MatrixXd();
    m_cov_query_query = Eigen::MatrixXd();
}


void GaussianProcess::recalculate_cov_train_train()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_training.rows() > 0 && m_training.cols() > 0)
        {
            m_cov_train_train =
                m_cov_func->cov(m_training) +
                m_noise_std * m_noise_std *
                Eigen::MatrixXd::Identity(m_training.rows(), m_training.rows());
            m_cholesky_train_train =
                m_cov_train_train.selfadjointView<Eigen::Upper>().llt().matrixU();
        }
        else
        {
            clear_training_data();
        }
    }
}

GPResult GaussianProcess::inference()
{
    Eigen::VectorXd kss(m_query.rows());
    for(int i=0; i<m_query.rows(); ++i)
    {
        kss(i) = get_cov_func()->cov(m_query.row(i)).value();
    }

    Eigen::MatrixXd ks = get_cov_func()->cov(get_training_data(), m_query);

    // Compute inverse
    Eigen::MatrixXd L = get_cholesky_train_train();
    Eigen::MatrixXd alpha = L.triangularView<Eigen::Upper>()
        .transpose()
        .solve(get_training_labels() -
               get_mean_func()->mean(get_training_data()));
    L.triangularView<Eigen::Upper>().solveInPlace(alpha);

    // Compute predictive mean and variance
    GPResult result;
    result.mean = get_mean_func()->mean(m_query) +
            (ks.transpose() * alpha);
    Eigen::MatrixXd v = L.triangularView<Eigen::Upper>().transpose().solve(ks);
    result.variance =
        kss - v.array().square().colwise().sum().transpose().matrix();

    return result;
}

double GaussianProcess::evaluate_goal_function(
        AbstractCovarianceFunction::ParamVec_t const& params,
        double noise)
{
    switch(m_goal_func_type)
    {
        case GF_LML:
        {
            unsigned int N = get_training_data().rows();

            Eigen::MatrixXd K = get_cov_func()->cov(get_training_data(), params);
            Eigen::MatrixXd L = (K+(noise*noise)*Eigen::MatrixXd::Identity(N,N));

            //FIXME (Roman) Why are we not considering the mean of the values,
            //              and assuming zero?
            //              Because of the mean function?
            double yMean = get_training_labels().sum()/N;
            //Eigen::VectorXd data_meanu= gp.mean(gp.get_training_data());
            Eigen::MatrixXd yMatrix = Eigen::ArrayXd(get_training_labels()
                                                     .transpose())-yMean;
            Eigen::LLT<Eigen::MatrixXd> llt;
            llt.compute(L);

            Eigen::MatrixXd alpha;
            alpha = llt.solve(yMatrix);

            double llt_diagonal_sum = 0;

            for(unsigned int i = 0 ; i < (unsigned int)llt.matrixLLT().rows() ; i++)
            {
                llt_diagonal_sum += log(llt.matrixLLT()(i,i));
            }

            Eigen::MatrixXd lml1 = -0.5*((yMatrix.transpose())*alpha);
            double lml2 = - llt_diagonal_sum;
            double lml3 = -0.5*N*log(2*M_PI);
            double lml = lml1(0,0)+lml2+lml3;

            return lml;
            break;
        }
        case GF_LOOCV:
        {
            throw(BaseError("Leave One Out Cross Validation not implemented yet."));
            break;
        }
    }
    throw(BaseError("Goal function not recognized."));
    return 0;
}

OptimiserParams GaussianProcess::get_optimiser_params()
{
    return get_cov_func()->get_optimiser_params();
}

void GaussianProcess::recalculate_cov_query_train()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_training.rows() > 0 && m_query.rows() > 0)
        {
            m_cov_query_train = m_cov_func->cov(m_training, m_query);
        }
    }
}


void GaussianProcess::recalculate_cov_query_query()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_query.rows() > 0)
        {
            m_cov_query_query = Eigen::VectorXd(m_query.rows());

            for(int i=0; i<m_query.rows(); ++i)
            {
                m_cov_query_query(i) = m_cov_func->cov(m_query.row(i)).value();
            }
        }
    }
}


void GaussianProcess::set_training_labels(Eigen::VectorXd const& labels)
{
    if(labels.rows() != m_training.rows())
    {
        throw DimensionError("Dimension mismatch between training data "
                             "and training labels.");
    }
    m_labels = labels;
}

} /* gp */
