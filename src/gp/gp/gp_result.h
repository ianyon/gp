#ifndef __GP_GP_RESULT_H__
#define __GP_GP_RESULT_H__


#include <Eigen/Core>


namespace gp
{
    
/**
 * \brief Stores the inference result of a Gaussian process.
 */
class GPResult
{
    public:
        //! The predicted mean values
        Eigen::ArrayXXd                 mean;
        //! The variances associated with the predicted means
        Eigen::ArrayXXd                 variance;
        //! The output of the goal function
        double                          goal_function;
};

} /* gp */

#endif /* __GP_GP_RESULT_H__ */
