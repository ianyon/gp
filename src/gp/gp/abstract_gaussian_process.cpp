#include <gp/gp/abstract_gaussian_process.h>

namespace gp
{

class {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
} nullPtr;

AbstractGaussianProcess::AbstractGaussianProcess()
    :m_cov_func(nullPtr)
    ,m_mean_func(nullPtr)
    ,m_goal_func_type(GF_LML)
{

}

AbstractGaussianProcess::AbstractGaussianProcess(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func
)
    :m_cov_func(cov_func)
    ,m_mean_func(mean_func)
    ,m_goal_func_type(GF_LML)
{

}

void AbstractGaussianProcess::set_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        m_cov_func->set_hyper_params(params);
    }
}

AbstractCovarianceFunction::ParamVec_t
            AbstractGaussianProcess::get_hyper_params()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        return m_cov_func->get_hyper_params();
    }
    else
    {
        throw(BaseError("Requesting parameters of a non existing covariance"
                        "function."));
    }
    return AbstractCovarianceFunction::ParamVec_t();
}

void AbstractGaussianProcess::set_free_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        m_cov_func->set_free_hyper_params(params);
    }
}

AbstractCovarianceFunction::ParamVec_t
            AbstractGaussianProcess::get_free_hyper_params()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        return m_cov_func->get_free_hyper_params();
    }
    else
    {
        throw(BaseError("Requesting parameters of a non existing covariance"
                        "function."));
    }
    return AbstractCovarianceFunction::ParamVec_t();
}

void AbstractGaussianProcess::set_noise(
        double const&  noise_std
)
{
    m_noise_std = noise_std;
}


double AbstractGaussianProcess::get_noise()
{
    return m_noise_std;
}

void AbstractGaussianProcess::set_query_points(
        Eigen::MatrixXd const&          query_points
)
{
    //Directly assign query point matrix
    m_query = query_points;
}

Eigen::MatrixXd & AbstractGaussianProcess::get_query_points()
{
    return m_query;
}

void AbstractGaussianProcess::clear_query_points()
{
    m_query = Eigen::MatrixXd();
}

GPResult AbstractGaussianProcess::inference(Eigen::MatrixXd const& query_points)
{
    set_query_points(query_points);
    return inference();
}

double AbstractGaussianProcess::evaluate_goal_function()
{
    return evaluate_goal_function(get_hyper_params(),get_noise());
}

boost::shared_ptr<AbstractCovarianceFunction>
    AbstractGaussianProcess::get_cov_func()
{
    return m_cov_func;
}

boost::shared_ptr<AbstractMeanFunction> AbstractGaussianProcess::get_mean_func()
{
    return m_mean_func;
}

void AbstractGaussianProcess::set_cov_func(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
{
    m_cov_func = cov_func;
}

void AbstractGaussianProcess::set_mean_func(
        boost::shared_ptr<AbstractMeanFunction> mean_func)
{
    m_mean_func = mean_func;
}

} /* gp */
