#ifndef __GP_GAUSSIAN_PROCESS_H__
#define __GP_GAUSSIAN_PROCESS_H__

#include <gp/gp/abstract_gaussian_process.h>

namespace gp
{

/**
 * \brief Gaussian Process.
 *
 * The class stores the different parts of the covariance matrix used for
 * regression separately for performance reasons.
 *
 * \f[
 * K = 
 * \left[
 * \begin{array}{cc}
 * cov(train, train) & cov(train, query) \\
 * cov(query, train) & cov(query, query)
 * \end{array}
 * \right]
 * \f]
 *
 * This is the structure of the overall covariance matrix and the three
 * distinct parts are stored separately. \f$train\f$ refers to the training
 * data points which are used to perform inference and to find the optimal
 * hyper parameters. \f$query\f$ refers to the data points at which the
 * Gaussian process should infer values.
 */

class GaussianProcess
        : public AbstractGaussianProcess
{
    public:

        GaussianProcess();

        GaussianProcess(boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                        boost::shared_ptr<AbstractMeanFunction> mean_func);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_free_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the data and labels of the GP to the given values.
         *
         * Each row of the data matrix is assumed to contain a single
         * example  with the same row of the label vector containing the
         * associated label.
         *
         * \param data the new training data to use
         * \param labels the labels corresponding to the data entries
         */
        void set_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Sets the labels of the training data.
         *
         * This method only changes the labels without changing the training
         * data itself.
         *
         * \param labels new labels for the training data
         */
        void set_training_labels(Eigen::VectorXd const& labels);

        /**
         * \brief Appends to given data to the existing data.
         *
         * \param data the data entries to add
         * \param labels the labels of the entries to be added
         */
        void append_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Removes the last data point.
         */
        void remove_last_training_point();

        void set_noise(double const& noise_std);

        void clear_query_points();

        void clear_training_data();

        Eigen::MatrixXd const& get_training_data() const
        {
            return m_training;
        }
        Eigen::MatrixXd & get_training_data()
        {
            return m_training;
        }
        Eigen::VectorXd const& get_training_labels() const
        {
            return m_labels;
        }
        Eigen::VectorXd & get_training_labels()
        {
            return m_labels;
        }

        /**
         * \brief Returns the covariance between training and training data.
         *
         * \return covariance between training and training data
         */
        Eigen::MatrixXd const& get_cov_train_train() const
        {
            return m_cov_train_train;
        }
        Eigen::MatrixXd & get_cov_train_train()
        {
            return m_cov_train_train;
        }

        /**
         * \brief Returns the covariance between query and training data.
         * 
         * \return covariance between query and training data
         */
        Eigen::MatrixXd const& get_cov_query_train() const
        {
            return m_cov_query_train;
        }
        Eigen::MatrixXd & get_cov_query_train()
        {
            return m_cov_query_train;
        }

        /**
         * \brief Returns the covariance between querey and query data.
         *
         * \return covariance between query and query data
         */
        Eigen::MatrixXd const& get_cov_query_query() const
        {
            return m_cov_query_query;
        }
        Eigen::MatrixXd & get_cov_query_query()
        {
            return m_cov_query_query;
        }

        /**
         * \brief Returns the Cholesky factorization of the training data covariance.
         *
         * \return Cholesky factorization of the training data covariance matrix
         */
        Eigen::MatrixXd const& get_cholesky_train_train() const
        {
            return m_cholesky_train_train;
        }
        Eigen::MatrixXd & get_cholesky_train_train()
        {
            return m_cholesky_train_train;
        }

        /**
         * \brief Performs inference and returns mean and variance.
         *
         * \param query_points the qyery points to infer values for
         * \return inference results
         */
        GPResult inference();

        double evaluate_goal_function(
                AbstractCovarianceFunction::ParamVec_t const& params,
                double noise);

        OptimiserParams get_optimiser_params();

    private:
        /**
         * \brief Recomputes the covariance matrix of the training data.
         */
        void recalculate_cov_train_train();
        /**
         * \brief Recomputes the covariance matrix between query and training data.
         */
        void recalculate_cov_query_train();
        /**
         * \brief Recomputes the covariance matrix of the query data.
         */
        void recalculate_cov_query_query();

    private:
        //! Training data
        Eigen::MatrixXd                 m_training;
        //! Labels for the training data
        Eigen::VectorXd                 m_labels;

        //! Covariance matrix for the train-train part of the covariance matrix
        Eigen::MatrixXd                 m_cov_train_train;
        //! Covariance matrix for the query-train part of the covariance matrix
        Eigen::MatrixXd                 m_cov_query_train;
        //! Covariance matrix for the query-query part of the covariance matrix
        Eigen::MatrixXd                 m_cov_query_query;
        //! Cholesky factorization of K matrix
        Eigen::MatrixXd                 m_cholesky_train_train;
};

} /* gp */

#endif /* __GP_GAUSSIAN_PROCESS_H__ */
