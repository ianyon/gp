#ifndef __GP_ABSTRACT_GAUSSIAN_PROCESS_H__
#define __GP_ABSTRACT_GAUSSIAN_PROCESS_H__

#include <vector>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>

#include <gp/gp/error.hpp>
#include <gp/gp/gp_result.h>
#include <gp/optimisers/optimiser_params.h>
#include <gp/covariance_functions/abstract_covariance_function.h>
#include <gp/mean_functions/abstract_mean_function.h>

namespace gp
{

enum GoalFunctionType
{
    GF_LML = 0,
    GF_LOOCV
};

/**
 * \brief Abstract Gaussian Process is the base class for all types of gaussian
 * processes.
 *
 * - CovFunc is the covariance function used by the GP
 * - MeanFunc is the mean function used by the GP
 *
 * The form of storage of information and handling of this data depends on each
 * type of GP.
 *
 * The main function to be implemented by each type is "inference",
 * i.e. calculating the predictive distribution over a set of query points.
 * Depending on the type of GP, each one does this differently, for example,
 *
 * Normal storage of observations as point coordintes:
 * Large Data GP only uses nearest neighbors approach to select training points
 * and only invert a subset of the large covariance matrix.
 * GP_simple stores the covariance matrix and only recalculates it when the
 * training data is modified (If the number of data points is greater that a
 * threshold the process of inverting the covariance matrix will become very
 * slow).
 *
 * Generic observation GPs use observations over lines, paths or surfaces to
 * handle observations over continuous domains.
 *
 */

class AbstractGaussianProcess
{
    public:

        AbstractGaussianProcess();

        AbstractGaussianProcess(
                        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                        boost::shared_ptr<AbstractMeanFunction> mean_func
        );

        virtual ~AbstractGaussianProcess() {}

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix.
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        AbstractCovarianceFunction::ParamVec_t get_hyper_params();

        /**
         * \brief Sets the free hyper parameters of the covariance function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix.
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_free_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        AbstractCovarianceFunction::ParamVec_t get_free_hyper_params();

        /**
         * \brief Sets the noise std of the data.
         *
         * It is assumed that the target values are noisy, where the noise
         * comes from N(0, s), a Gaussian with zero mean and sigma. The sigma
         * paramerter is set using this method.
         *
         * \param noise the new noise value, interpreted as standard deviation
         */
        virtual void set_noise(double const& noise);

        /**
         * \brief Gets the value of the noise std stored for the gp.
         *
         * \return the stored noise value, interpreted as the standard deviation
         */
        double get_noise();

        /**
         * \brief Sets the query points.
         *
         * The covariance between the query points can be provided by the user
         * if it is available.
         *
         * \param query_points the points for which to make predictions
         * \param covariance covariance matrix of the query data
         */
        void set_query_points(Eigen::MatrixXd const&  query_points);

        Eigen::MatrixXd & get_query_points();

        /**
         * \brief Clears all current test points.
         */
        virtual void clear_query_points();

    public:
        /**
         * \brief Clears all stored training data.
         *
         * Because each GP stores training data of different type, each GP
         * knows how to clear its own training data.
         * For example, integral line GPs store data as lines not as points.
         */
        virtual void clear_training_data() = 0;

        /**
         * \brief Performs inference and returns mean and variance.
         *
         * \param query_points the qyery points to infer values for
         * \return inference results
         */        
        virtual GPResult inference() = 0;

        GPResult inference(Eigen::MatrixXd const& query_points);

        /**
         * \brief Evaluates a goal function depending on the data, and the
         * hyperparameters.
         *
         * The parameters and noise are explicitely given, if no parameters are
         * provided, then this function is called using the ones stored in the
         * gp structure.
         *
         * \param params Parameter vector of the covariance function
         * \param noise Noise standard deviation
         * \return goal_function score
         */
        virtual double evaluate_goal_function(
                AbstractCovarianceFunction::ParamVec_t const& params,
                double noise) = 0;

        double evaluate_goal_function();

        virtual OptimiserParams get_optimiser_params() = 0;

    public:
        boost::shared_ptr<AbstractCovarianceFunction> get_cov_func();

        boost::shared_ptr<AbstractMeanFunction> get_mean_func();

        void set_cov_func(
                boost::shared_ptr<AbstractCovarianceFunction> cov_func);

        void set_mean_func(
                boost::shared_ptr<AbstractMeanFunction> mean_func);

    protected:
        boost::shared_ptr<AbstractCovarianceFunction> m_cov_func;

        boost::shared_ptr<AbstractMeanFunction> m_mean_func;

        //! Gaussian distributed noise standard deviation.
        double           m_noise_std;

        //! Query points
        Eigen::MatrixXd  m_query;

        //! Type of goal function to be optimised
        GoalFunctionType m_goal_func_type;
};

} /* gp */

#endif /* __GP_ABSTRACT_GAUSSIAN_PROCESS_H__ */
