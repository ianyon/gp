#ifndef __GP_SIMULATED_ANNEALING__
#define __GP_SIMULATED_ANNEALING__

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>

#include <Eigen/Core>

#include <gp/gp/abstract_gaussian_process.h>
#include <gp/optimisers/optimiser_params.h>


namespace gp
{

/**
 * \brief Optimiser using simulated annealing.
 *
 * This class implements optimization using simulated annealing.
 */
template<typename GP>
class SimulatedAnnealing
{
    public:
        SimulatedAnnealing(double cooling_factor=0.8);

        /**
         * \brief Performs the optimisation on the given data.
         *
         * \param gp the gaussian process to optimize the parameters of
         * \param optimiser_params the parameters to use in the optimisation
         */
        void optimise(GP & gp);

        /**
         * \brief Returns the name of the optimiser.
         *
         * \return name of the optimizer
         */
        std::string get_name() const;

        /**
         * \brief Returns the optimal covariance function parameters
         *
         * \return optimal covariance function parameters
         */
        std::vector<double> get_optimal_cov_func_params() const;

        /**
         * \brief Returns the optimal noise
         *
         * \return optimal noise
         */
        double get_optimal_noise() const;

    private:
        /**
         * \brief Generates a new set of neighbouring parameters from the old
         *
         * \param temperature current temperature
         * \param params current hyperparatemers of the covariance function
         * \param noise current noise
         * \param newparams vector to store the new neighbouring hyperparameters
         * \param newnoise variable to store the new noise parameter
         **/
        void newsol(double temperature, std::vector<double> const& params, double noise, std::vector<double>& newparams, double& newnoise);

        /**
         * \brief Lowers the temperature
         *
         * \param temp the current temperature
         * \return the reduced temperature
         **/
        double cool(double temp);

        /**
         * \brief Generate a uniformly distributed random number over the interval [0,1]
         *
         * \return random number
         **/
        double ranf();

        /**
         * \brief Generate a standard Gaussian distributed random number
         *
         * \return Gaussian random number
         **/
        double randn();

        std::vector<double> m_optimal_cov_func_params;
        double m_optimal_noise;

        //! Factor by which to reduce the temperature on each iteration
        double                          m_cooling_factor;
        //! Random number generator
        boost::mt19937          m_random_number_gen;
};


template<typename GP>
SimulatedAnnealing<GP>::SimulatedAnnealing(double cooling_factor)
    :   m_cooling_factor(cooling_factor)
{
    m_random_number_gen.seed(std::time(0));
}

template<typename GP>
void SimulatedAnnealing<GP>::optimise(GP & gp)
{
    OptimiserParams optimiser_params = gp.get_optimiser_params();
    bool minimise = true;

    if(optimiser_params.get_parameter(OptimiserParams::GOAL) ==
            OptimiserParams::MAXIMISE
       )
    {
        minimise = false;
    }
    else
    {
        minimise = true;
    }

    double T = 1.0;
    double minT = 0.001;

    std::vector<double> par = gp.get_cov_func()->get_free_hyper_params();
    double noise = gp.get_noise();
    m_optimal_cov_func_params = par;
    m_optimal_noise = noise;


    double newenergy = 0;
    double oldenergy = minimise ? gp.evaluate_goal_function(
                                              m_optimal_cov_func_params,
                                              m_optimal_noise)
                                : - gp.evaluate_goal_function(
                                              m_optimal_cov_func_params,
                                              m_optimal_noise);
    double newnoise = 0;
    std::vector<double> newpar;

    int itry = 0;
    int max_try = 300;
    int success = 0;
    int max_success = 20;
    int consec = 0;
    int max_consec_rejections = 1000;

    std::cout << "Starting Simulated Annealing\nInital parameters: ";
    for(std::vector<double>::const_iterator it = par.begin() ; it != par.end(); it++)
    {
        std::cout << *it << ", ";
    }
    std::cout << noise;
    std::cout << "\nInitial temperature = " << T << std::endl;


    // Uniform [0, 1] distribution
    boost::uniform_real<> uniform_dist(0.0, 1.0);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<> >
            uniform_dist_gen(m_random_number_gen,uniform_dist);

    while(1)
    {
        ++itry;
        par = m_optimal_cov_func_params;
        noise = m_optimal_noise;

        if ((itry >= max_try) || success >= max_success)
        {
            if ((T < minT) || consec >= max_consec_rejections)
            {
                break;
            }
            else
            {
                T = cool(T);
                std::cout << "Lowering temperature. New temperature = " << T << std:: endl;
                itry = 1;
                success = 1;
            }
        }

        newsol(T, par,noise,newpar,newnoise);
        newenergy = minimise ? gp.evaluate_goal_function(newpar,newnoise)
                             : -gp.evaluate_goal_function(newpar,newnoise);

        if ((oldenergy - newenergy) > 1e-6)
        {
            m_optimal_cov_func_params = newpar;
            m_optimal_noise = newnoise;
            oldenergy = newenergy;
            ++success;
            consec = 0;
        }
        else
        {
            if (exp((oldenergy-newenergy)/T) > uniform_dist_gen())
            {
                m_optimal_cov_func_params = newpar;
                m_optimal_noise = newnoise;
                oldenergy = newenergy;
                ++success;
            }
            else
            {
                ++consec;
            }
        }
    }

    std::cout << "Optimising done.\nFinal parameters: ";
    for(std::vector<double>::const_iterator it = m_optimal_cov_func_params.begin() ; it != m_optimal_cov_func_params.end(); it++)
    {
        std::cout << *it << ", ";
    }
    std::cout << m_optimal_noise << std::endl;

    // Update GP parameters
    gp.set_free_hyper_params(m_optimal_cov_func_params);
    gp.set_noise(m_optimal_noise);
}

template<typename GP>
std::string SimulatedAnnealing<GP>::get_name() const
{
    return "Simulated annealing";
}

template<typename GP>
std::vector<double> SimulatedAnnealing<GP>::get_optimal_cov_func_params() const
{
    return m_optimal_cov_func_params;
}

template<typename GP>
double SimulatedAnnealing<GP>::get_optimal_noise() const
{
    return m_optimal_noise;
}

template<typename GP>
void SimulatedAnnealing<GP>::newsol(
        double                          temperature,
        std::vector<double> const&      params,
        double                          noise,
        std::vector<double> &           new_params,
        double &                        new_noise
)
{
    new_noise = noise;
    new_params = params;

    boost::uniform_int<> uniform_dist(0, params.size());
    boost::normal_distribution<> normal_dist(0.0, 1.0);

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> >
            uniform_dist_gen(m_random_number_gen,uniform_dist);

    
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<> >
            normal_dist_gen(m_random_number_gen,normal_dist);

    int id = uniform_dist_gen();
    if(id < static_cast<int>(params.size()))
    {
        new_params[id] *=
            (1.0 + temperature * normal_dist_gen());
    }
    else
    {
        new_noise *= 1.0 + temperature * normal_dist_gen();
    }
}

template<typename GP>
double SimulatedAnnealing<GP>::cool(double temp)
{
    return temp * m_cooling_factor;
}

} /* gp */

#endif /* __GP_SIMULATED_ANNEALING__ */
