#include <gp/optimisers/nlopt_optimiser.h>
#include <iterator>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>

namespace gp
{


//! Callback Function.
//! Very important function because it is a trampolyn function so that the
//! optimiser library can call a member function of NLoptOptimiser to be
//! optimised, and use information from the class.

double callback(
        std::vector<double> const&      x,
        std::vector<double> &           grad,
        void *                          func_ptr
)
{
    typename NLoptOptimiser::OptFunc_t func =
            *static_cast<typename NLoptOptimiser::OptFunc_t*>(func_ptr);
    return func(x, grad);
}

NLoptOptimiser::NLoptOptimiser()
    :m_internal_grad(true)
	,m_algorithm_id(nlopt::LN_COBYLA)
	,m_nlopt_dim(0)
    ,m_iterations(0)
	,m_verbose(false)
{

}

void NLoptOptimiser::optimise(AbstractGaussianProcess & gp)
{
    //Fill the information for the optimiser params.
    OptimiserParams optimiser_params = gp.get_optimiser_params();

    //problem sizes
    m_nlopt_dim = optimiser_params.get_parameter(
                OptimiserParams::NUMBER_OF_UNKNOWNS)+1; //Plus noise

    m_opt = boost::shared_ptr<nlopt::opt>(
                new nlopt::opt(m_algorithm_id,(unsigned int)m_nlopt_dim));

    sol_initial.clear();
    std::vector<double> hyper_params = gp.get_cov_func()->get_free_hyper_params();
    if(hyper_params.size() + 1 == m_nlopt_dim)
    {
        //Fill the first elements of hyperparameters with the covariance
        //hyperparameters.
        for (unsigned int i = 0; i < (unsigned int)m_nlopt_dim -1 ; i++)
        {
            sol_initial.push_back(hyper_params[i]);
        }
        //Fill the last one with the noise parameter.
        sol_initial.push_back(gp.get_noise());
    }
    else
    {
        throw DimensionError("NLOpt hyperameter count does not match the "
                             "dimension of the problem.");
    }

    OptFunc_t func = boost::bind(&NLoptOptimiser::objective_func,
                                 this, _1, _2, boost::ref(gp));
    m_opt->set_max_objective(callback, &func);

    m_opt->set_xtol_rel(1e-4);

    double minf;

	if (m_logging_file_name.length()>0)
		logging_file_stream.open(m_logging_file_name,std::ios_base::app);

    nlopt::result result = m_opt->optimize(sol_initial, minf);

	if (logging_file_stream.is_open())
		logging_file_stream.close();

    if (result < 0)
    {
        throw OptimiserError("NLopt: Optimisation Failed");
    }

    m_results.m_optimal_cov_func_params.clear();
    m_results.m_optimal_noise = 0;
    if(hyper_params.size() + 1 == m_nlopt_dim)
    {
        for (unsigned int i = 0; i < (unsigned int)m_nlopt_dim ; i++)
        {
            if ( i != (unsigned int)(m_nlopt_dim - 1) )
            {
                m_results.m_optimal_cov_func_params.push_back(sol_initial[i]);
            }
            else
            {
                m_results.m_optimal_noise = sol_initial[i];
            }
        }
    }
    else
    {
        throw DimensionError("NLoptOptimiser: Mismach of parameters to be "
                             "evaluated in goal function.");
    }

    // Update GaussianProcess  parameters
    gp.set_free_hyper_params(m_results.m_optimal_cov_func_params);
    gp.set_noise(m_results.m_optimal_noise);

    std::cout<<"Found result: f(";
    std::vector<double> optimal_params = gp.get_cov_func()->get_hyper_params();
    optimal_params.push_back(gp.get_noise());
    for(unsigned int i = 0; i < optimal_params.size(); i++)
    {
        if(i == (optimal_params.size() - 1))
        {
            std::cout<<"noise = "<<optimal_params[i]<<") = ";
        }
        else
        {
            std::cout<<optimal_params[i]<<",";
        }
    }
    std::cout<<minf<<std::endl;
}


double NLoptOptimiser::objective_func(const std::vector<double> &x,
                                      std::vector<double>       &grad,
                                      AbstractGaussianProcess   &gp
)
{
    m_iterations++;
    //transform x to params and noise
    std::vector<double> hyper_params = gp.get_cov_func()->get_free_hyper_params();
    if(hyper_params.size() + 1 == m_nlopt_dim)
    {
        std::vector<double> params;
        double noise = 0;
        for (unsigned int i = 0; i < (unsigned int)m_nlopt_dim ; i++)
        {
            if ( i != (unsigned int)(m_nlopt_dim - 1) )
            {
                params.push_back(x[i]);
            }
            else
            {
                noise = x[i];
            }
        }
        double value = gp.evaluate_goal_function(params,noise);
        if(m_verbose)
        {
            if(m_iterations == 1)
            {
                std::cout<<"Iterations\t"<<"Params\t"<<"Objective"<<std::endl;
            }
            std::cout<<m_iterations<<"\t";
            for(unsigned int k = 0; k < params.size(); k++)
            {
                if( k == 0 )
                {
                    std::cout<<"("<<params[k];
                }
                else if( k == params.size()-1 )
                {
                    std::cout<<","<<params[k]<<","<<noise<<")";
                }
                else
                {
                    std::cout<<","<<params[k];
                }
            }
            std::cout<<"\t"<<value<<std::endl;
        }
		if(logging_file_stream.is_open())
        {
            logging_file_stream<<m_iterations<<"\t";
			


			copy(
                    params.begin(),
                    params.end(),
                    std::ostream_iterator<double>( logging_file_stream , "\t")
					);


			logging_file_stream<<noise<<"\n";
        }
        return value;
    } else
    {
        throw DimensionError("NLoptOptimiser: Mismach of parameters to be "
                             " evaluated in goal function.");
    }
}


std::string NLoptOptimiser::get_name() const
{
    return "NLopt";
}


void NLoptOptimiser::set_verbose(bool verbose)
{
    m_verbose = verbose;
}


std::vector<double> NLoptOptimiser::get_optimal_cov_func_params() const
{
    return m_results.m_optimal_cov_func_params;
}


double NLoptOptimiser::get_optimal_noise() const
{
    return m_results.m_optimal_noise;
}


void NLoptOptimiser::set_grad_calculation_internal(
        bool calc_grad_internally)
{
    m_internal_grad = calc_grad_internally;
}

void NLoptOptimiser::set_algorithm(nlopt::algorithm algorithm_id)
{
    m_algorithm_id = algorithm_id;
}

void NLoptOptimiser::set_logging_file_name(std::string logging_file_name)
{
	m_logging_file_name = logging_file_name;
}

} /* gp */

