#include <gp/optimisers/optimiser_params.h>


namespace gp
{
    
void OptimiserParams::set_constraint(
        unsigned int                    index,
        Constraint                      constraint,
        double                          value
)
{
    if(index >= m_constraints.size())
    {
        m_constraints.resize(index+1);
        m_params.resize(index+1);
    }

    m_constraints[index].insert(std::make_pair(constraint, value));
}

void OptimiserParams::set_parameter(Parameters index, int value)
{
    if((unsigned int)index >= m_params.size())
    {
        m_constraints.resize(index+1);
        m_params.resize(index+1);
    }
    
    m_params[index] = value;
}

int OptimiserParams::get_parameter(Parameters index)
{
    return m_params[index];
}

OptimiserParams::ConstraintsMap_t OptimiserParams::get_constraints() const
{
    return m_constraints;
}

OptimiserParams::ParameterVec_t OptimiserParams::get_parameters() const
{
    return m_params;
}

OptimiserParams OptimiserParams::merge(OptimiserParams const& other) const
{
    OptimiserParams op = *this;
    std::copy(
            other.m_params.begin(),
            other.m_params.end(),
            std::back_inserter(op.m_params)
    );
    std::copy(
            other.m_constraints.begin(),
            other.m_constraints.end(),
            std::back_inserter(op.m_constraints)
    );

    return op;
}

} /* gp */
