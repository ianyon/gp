#ifndef __GP_OPTIMISER_PARAMS_H__
#define __GP_OPTIMISER_PARAMS_H__


#include <map>
#include <vector>


namespace gp
{
    
/**
 * \brief Parameters given to the optimiser that define the goal function.
 *
 * This class serves mainly as a structure, used by the optimiser and filled by
 * the covariance function that defines the problem to be optimised, i.e. the
 * number of parameters to be optimised, restrictions, presence of gradients
 * and so on.
 */
class OptimiserParams
{
    public:
        /**
         * \brief Enumeration of the type of goal
         */
        enum GoalType
        {
            MAXIMISE = 0,
            MINIMISE
        };

        /**
         * \brief Enumeration of the parameters in the array of parameters
         */
        enum Parameters
        {
            NUMBER_OF_UNKNOWNS = 1,
            NUMBER_OF_CONSTRAINTS,
            KNOWN_DERIVATIVES,
            GOAL
        };

        /**
         * \brief Enumeration of the possible optimisation constraints.
         */
        enum Constraint
        {
            LESS_THEN = 1,
            LESS_OR_EQUAL_THEN,
            GREATHER_THEN,
            GREATHER_OR_EQUAL_THEN,
            MAXIMUM_VALUE,
            MINIMUM_VALUE,
            COUNT
        };

        //! Typedef for constraint storage structure
        typedef std::vector<std::map<Constraint, double> > ConstraintsMap_t;
        //! Typedef for the storage of parameters
        typedef std::vector<int> ParameterVec_t;


    public:
        /**
         * \brief Sets a constraint for a parameter.
         *
         * \param index the index of the parameter for which to set a constraint
         * \param constraint they type of constraint to set
         * \param value the value of the constraint
         */
        void set_constraint(
                unsigned int            index,
                Constraint              constraint,
                double                  value
        );

        /**
         * \brief Sets the value of a parameter.
         *
         * \param index the index of the parameter to set the value of
         * \param value the value to set for the given parameter
         */
        void set_parameter(Parameters index, int value);

        /**
         * \brief Returns the value of a parameter.
         *
         * \param index the index of the parameter to set the value of
         * \return value the value to set for the given parameter
         */
        int get_parameter(Parameters index);
        
        /**
         * \brief Returns all stored constraints.
         *
         * \return all the stored constraints
         */
        ConstraintsMap_t get_constraints() const;

        /**
         * \brief Returns the values for all parameters.
         *
         * \return all the stored parameter values
         */
        ParameterVec_t get_parameters() const;

        OptimiserParams merge(OptimiserParams const& other) const;

    private:
        //! Storage for the optimisation constraints
        ConstraintsMap_t                m_constraints;
        //! Storage for the optimiser parameter values
        ParameterVec_t                  m_params;
};

} /* gp */

#endif /* __GP_OPTIMISER_PARAMS_H__ */
