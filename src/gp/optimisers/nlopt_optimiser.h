#ifndef __GP_NLOPT_OPTIMISER_HPP__
#define __GP_NLOPT_OPTIMISER_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include <Eigen/Core>
#include <nlopt.hpp>

#include <gp/gp/abstract_gaussian_process.h>
#include <gp/optimisers/optimiser_params.h>

namespace gp
{

/**
 * \brief NLopt optimiser.
 *
 * This class implements communication with a NLopt optimiser
 * Non-linear optimization library.
 * http://ab-initio.mit.edu/wiki/index.php/NLopt
 *
*/


class NLoptOptimiser
{
    public:
        typedef boost::function<double (std::vector<double> const&,
                                        std::vector<double> &)> OptFunc_t;

        struct Results
        {
            //! Results that stores optimal cov_func_params
            std::vector<double> m_optimal_cov_func_params;

            //! Results that stores optimal noise std for the gp
            double m_optimal_noise;
        };

	public:

        /**
          * \brief NLopt constructor.
          *
          */
        NLoptOptimiser();

        /**
         * \brief Performs the optimisation on the given data.
         *
         * \param gp the gaussian process to optimize the parameters of
         * \param optimiser_params the parameters to use in the optimisation
         */
        void optimise(AbstractGaussianProcess  & gp);

        /**
         * \brief Returns the name of the optimiser.
         *
         * \return name of the optimizer
         */
        std::string get_name() const;

        /**
         * \brief Returns the optimal covariance function parameters
         *
         * \return optimal covariance function parameters
         */
        std::vector<double> get_optimal_cov_func_params() const;

        /**
         * \brief Returns the optimal noise
         *
         * \return optimal noise
         */
        double get_optimal_noise() const;

        /**
         * \brief Sets the policy for gradient calculation (if the optimiser
         * calculates it inernally true or if it is given false)
         *
         * \param calc_grad_internally true if the optimiser calculates
         * gradient internally
         */
        void set_grad_calculation_internal(bool calc_grad_internally);


        void define_problem_size(OptimiserParams optimiser_params);

        void set_verbose(bool verbose);

        double objective_func(const std::vector<double> &x,
                              std::vector<double>       &grad,
                              AbstractGaussianProcess   &gp
        );

		void set_algorithm(nlopt::algorithm algorithm_id);
		void set_logging_file_name(std::string logging_file_name);


private:

        //! Results filled by the optimiser after completing the optimise
        //! function.
        Results m_results;

        //! Calculate gradient internally
        bool m_internal_grad;

        //! NLopt Optimiser pointer to object.
        //! It is a pointer to allow reinicialization of optimiser with
        //! different dimension if new hyper parameters are included
        //! in the goal function (for example). I.e. dynamically restructure
        //! the optimiser object.
        boost::shared_ptr<nlopt::opt> m_opt;

        //! NLopt algorithm;
        nlopt::algorithm m_algorithm_id;

		std::string m_logging_file_name;
		std::ofstream logging_file_stream;

        unsigned int m_nlopt_dim;
        std::vector<double> sol_initial;

        unsigned int m_iterations;
        bool m_verbose;
};

} /* gp */

#endif /* __GaussianProcess _NLOPT_OPTIMISER_HPP__ */
