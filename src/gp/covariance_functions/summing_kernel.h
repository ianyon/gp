#ifndef __GP_SUMMING_KERNEL_HPP__
#define __GP_SUMMING_KERNEL_HPP__

#include <vector>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>
#include <gp/optimisers/optimiser_params.h>
#include <gp/covariance_functions/abstract_covariance_function.h>


namespace gp
{

/**
 * \brief Summing Kernel covariance function.
 *
 * This class represents a summing kernel, it reimplements most of the functions
 * in an abstract covariance function because for each function it calls the
 * same function of the member array.
 *
 * The kernels to be summed are added to the cov_funcs array.
 */

class SummingKernel
        :public AbstractCovarianceFunction
{
    public:
        SummingKernel(unsigned int dim = 1);

        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Returns the number of hyperparameters for a given input
         * dimension
         *
         * \param dim the dimension of the input data
         * \return The number of hyperparameters
         **/
        unsigned int get_nr_of_params();

        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the covariance function has derivative
         * information
         */
        bool has_derivatives();

        /**
         * \brief Checks if the dimensionality of the data is coherent to what
         *        is expected by each covariance function.
         *
         * In case the dimensionality is not expected, an error will be thrown.
         *
         * \param x Matrix where rows are measurements and columns are
         *          dimensions.
         */
        void dimensionality_check(Eigen::MatrixXd const&  x);

        /**
         * \brief Checks if the dimensionality of the parameters is coherent
         *        to what is expected by each covariance function.
         *
         * In case that the dimensionality is not expected, an error will
         * be thrown.
         *
         * \param params Array of parameters for the covariance function.
         */
        void dimensionality_check(ParamVec_t const& params);


        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 First matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov(
                Eigen::MatrixXd const& x1,
                Eigen::MatrixXd const& x2
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov(Eigen::MatrixXd const& x1);

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return the covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i,
                bool                   free_params = true
        );

        /**
         * \brief Returns the derivative covariance matrix computed from two
         *        arrays of data.
         *
         * \param x1 First matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param deriv_i Index of the parameter from wich to calculate the
         *                derivative.
         * \return the covariance matrix computed from the given data
         */
        Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Sets the parameters of the function.
         *
         * The length of the input must be "complete", i.e. of the expected
         * size depending on each covariance function. The only depends on the
         * covariance function itself and does not depend on any remapped or
         * fixed parameters.
         *
         * \param params The parameters to be set.
         * \param force  Forces the hyperparameter setting, removing fixed and
         *               remapped parameters.
         */
        void set_hyper_params(
                                      ParamVec_t const& params,
                                      bool force = false
        );

        /**
         * \brief Sets the free parameters of the function.
         *
         * The length of the input must be only for free parameters, i.e
         * depends on the number of remapped and/or fixed parameters. Ideal for
         * with optimisers, because only free params can be modified.
         *
         * \param params The parameters to be set.
         */
         void set_free_hyper_params(ParamVec_t const& params);

         /**
          * \brief Returns the parameters of the function.
          *
          * The length of the output is "complete", i.e. of the expected
          * size depending on each covariance function. The output will include
          * fixed and remapped hyperparameters.
          *
          * \return Parameters of the function
          */
         ParamVec_t get_hyper_params() const;

         /**
          * \brief Returns the free parameters of the function.
          *
          * The length of the out constains only free parameters, i.e
          * depends on the number of remapped and/or fixed parameters.
          * Ideal for use with optimiser, as only returns free parameters.
          *
          * \return Free parameters of the function
          */
         ParamVec_t get_free_hyper_params();

         /**
          * \brief Returns the number of free parameters of the function.
          *
          * The output number depends only on free parameters, i.e
          * depends indirectly on the number of remapped and/or fixed parameters.
          * Ideal for use with optimiser, as only returns free parameters.
          *
          * \return Free parameters of the function
          */
         unsigned int get_nr_of_free_params();

    private:
          /**
           * \brief Fixes a param to a particular value.
           *
           * Function should not be called because each covariance function
           * member will fix and remmap their parameters internally.
           *
           * \param index Index of the parameter to be fixed (index in the range
           *              of total number of parameters), without considering
           *              previously remapped or clamped values.
           * \param value New value of the parameter.
           */
          void fix_param(unsigned int index, double value);

          /**
           * \brief Sets a remapping for the array of indexes.
           *
           * Function should not be called because each covariance function
           * member will fix and remmap their parameters internally.
           *
           * \param remapping_vector Vector that remaps the indexes of the
           *                         hyperparameters of a covariance function.
           */
          void set_remapping(std::vector<unsigned int> remapping_vector);

          /**
           * @brief Reset parameters of a covariance function.
           */
          void reset_params();

    public:
          void add_cov_func(
                  boost::shared_ptr<AbstractCovarianceFunction> cov_func);

    private:
        /**
         * @brief Returns a subset of the hyperparameters from the input vector
         *        params to the covariance function specified by the index.
         *
         * @param index Index of the covariance function whose subset of
         *              parameters you want to obtain.
         * @param params Full size of parameters you want to calculate the
         *               correspondance to.
         * @return The subset of parameters from params that corresponds to the
         *         covariance function of index "index".
         */
        ParamVec_t get_hyper_params_for_index(unsigned int const& index,
                                              ParamVec_t const& params);

        /**
         * @brief Returns a subset of the free hyperparameters of
         *        the covariance function specified by the index.
         *
         * @param index Index of the covariance function whose subset of
         *              parameters you want to obtain.
         * @param params Free parameter array.
         * @return The subset of free parameters from params that corresponds
         *         to the covariance function of index "index".
         */
        ParamVec_t get_free_hyper_params_for_index(unsigned int const& index,
                                                   ParamVec_t const& params);

    private:
        std::vector<boost::shared_ptr<AbstractCovarianceFunction> > cov_funcs;
};

} /* gp */

#endif /* __GP_SUMMING_KERNEL_HPP__ */
