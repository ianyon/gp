#ifndef __GP_POLYNOMIAL_HPP__
#define __GP_POLYNOMIAL_HPP__

#include <math.h>

#include <gp/covariance_functions/abstract_covariance_function.h>
#include <gp/gp/error.hpp>


namespace gp
{

template<unsigned int P>
class Polynomial;
    
/**
 * \brief Polynomial covariance function.
 *
 * This class represents the Polynomial covariance function.
 */
template<unsigned int P>
class Polynomial
        :public AbstractCovarianceFunction
{
    public:
        Polynomial(unsigned int dim = 1);
        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance matrix parameters
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance matrix parameters
         * \param deriv_i index of the param to calculate derivate
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the covariance function has derivative
         * information
         */
        bool has_derivatives();

        /**
         * \bried Returns the number of hyperparameters for a given input
         * dimension
         *
         * \return The number of hyperparameters
         **/
        unsigned int get_nr_of_params();
};

template<unsigned int P>
Polynomial<P>::Polynomial(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

template<unsigned int P>
Eigen::MatrixXd Polynomial<P>::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const& params
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).square();

    // Fill covariance matrix
    return (params[m_dim] *
            params[m_dim] +
            (x1*param_inv.asDiagonal()*x2.transpose()).array()).pow(P);

}

template<unsigned int P>
Eigen::MatrixXd Polynomial<P>::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_sq =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).square();

    double sigma = params[m_dim];

    double sigma_i;
    sigma_i = params[deriv_i];

    if (deriv_i < m_dim)
    {
        return 2*P*sigma_i*
                (x1.col(deriv_i)*x2.col(deriv_i).transpose()).array()*
                (sigma*sigma +
                 (x1*param_sq.asDiagonal()*x2.transpose()).array()).pow(P-1);

    }
    else if (deriv_i == m_dim)
    {
        return 2*P*sigma*(sigma*sigma +
                          (x1*param_sq.asDiagonal()*
                           x2.transpose()).array()).pow(P-1);
    }
    else
    {
        throw DerivativeError("Wrong derivative index requested, no such"
                              "parameter to derive with respect to.");
    }
}

template<unsigned int P>
bool Polynomial<P>::has_derivatives()
{
    return true;
}

template<unsigned int P>
unsigned int Polynomial<P>::get_nr_of_params()
{
    return m_dim + 1;
}

} /* gp */

#endif /* __GP_POLYNOMIAL_HPP__ */
