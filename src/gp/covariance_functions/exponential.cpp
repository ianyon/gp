#include <iostream>

#include <gp/covariance_functions/exponential.h>
#include <gp/gp/error.hpp>

namespace gp
{

Exponential::Exponential(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd Exponential::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        Exponential::ParamVec_t const& params
)
{
    // Create the diagonal lengthscale matrix
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();
    Eigen::MatrixXd sqdist = squared_distance(x1*param_inv.asDiagonal(),
                                              x2*param_inv.asDiagonal());
    Eigen::MatrixXd sqroot = sqdist.cwiseSqrt();
    return params[m_dim] * params[m_dim] * exp((-sqroot.array()));
}

Eigen::MatrixXd Exponential::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    // TODO: Calculate the expresion of the derivative and implement
    // this function.

    if(!has_derivatives())
    {
        throw DerivativeError("Derivative not implemented");
    }

    return  Eigen::MatrixXd();
}

unsigned int Exponential::get_nr_of_params()
{
    return m_dim + 1;
}

bool Exponential::has_derivatives()
{
    return false;
}

} /* gp */
