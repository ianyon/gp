#ifndef __GP_ABSTRACT_COVARIANCE_FUNCTION_H__
#define __GP_ABSTRACT_COVARIANCE_FUNCTION_H__

#include <vector>
#include <Eigen/Core>

#include <gp/optimisers/optimiser_params.h>

namespace gp
{

/**
 * \brief Generic covariance function.
 *
 * This class represents a generic covariance function. All generated covariance
 * functions must inherit from this covariance function to inherit methods for
 * clamping a specific hyperparameter or remapping them.
 *
 * It first evaluates a clamped parameters and then the remapping.
 */
class AbstractCovarianceFunction
{
    public:
        //! Typedef for the storage of parameters
        typedef std::vector<double> ParamVec_t;

    public:
        AbstractCovarianceFunction(unsigned int dim = 1);
        virtual ~AbstractCovarianceFunction();

    private:
        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd do_cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params
        ) = 0;

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd do_cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i
        ) = 0;

    public:

        /**
         * \brief Returns the number of hyperparameters for a given input
         * dimension
         *
         * \return The number of hyperparameters
         **/
        virtual unsigned int get_nr_of_params() = 0;

        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the covariance function has derivative
         * information
         */
        virtual bool has_derivatives() = 0;


        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 First matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const& x1,
                Eigen::MatrixXd const& x2
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(Eigen::MatrixXd const& x1);

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i,
                bool                   free_params = true
        );

        /**
         * \brief Returns the derivative covariance matrix computed from two
         *        arrays of data.
         *
         * \param x1 First matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param deriv_i Index of the parameter from wich to calculate the
         *                derivative.
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Checks if the dimensionality of the data is coherent to what
         *        is expected by the covariance function.
         *
         * In case the dimensionality is not expected, an error will be thrown.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param x Matrix where rows are measurements and columns are
         *          dimensions.
         */
        virtual void dimensionality_check(Eigen::MatrixXd const&  x);

        /**
         * \brief Checks if the dimensionality of the parameters is coherent
         *        to what is expected by the covariance function.
         *
         * In case that the dimensionality is not expected, an error will
         * be thrown.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param params Array of parameters for the covariance function.
         */
        virtual void dimensionality_check(ParamVec_t const& params);

        /**
         * \brief Sets the parameters of the function.
         *
         * The length of the input must be "complete", i.e. of the expected
         * size depending on each covariance function. The only depends on the
         * covariance function itself and does not depend on any remapped or
         * fixed parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param params The parameters to be set.
         * \param force  Forces the hyperparameter setting, removing fixed and
         *               remapped parameters.
         */
        virtual void set_hyper_params(
                                      ParamVec_t const& params,
                                      bool force = false
        );

        /**
         * \brief Sets the free parameters of the function.
         *
         * The length of the input must be only for free parameters, i.e
         * depends on the number of remapped and/or fixed parameters. Ideal for
         * with optimisers, because only free params can be modified.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param params The parameters to be set.
         */
        virtual void set_free_hyper_params(ParamVec_t const& params);

        /**
         * \brief Returns the parameters of the function.
         *
         * The length of the output is "complete", i.e. of the expected
         * size depending on each covariance function. The output will include
         * fixed and remapped hyperparameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Parameters of the function
         */
        virtual ParamVec_t get_hyper_params() const;

        /**
         * \brief Returns the free parameters of the function.
         *
         * The length of the out constains only free parameters, i.e
         * depends on the number of remapped and/or fixed parameters.
         * Ideal for use with optimiser, as only returns free parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Free parameters of the function
         */
        virtual ParamVec_t get_free_hyper_params();

        /**
         * \brief Returns the number of free parameters of the function.
         *
         * The output number depends only on free parameters, i.e
         * depends indirectly on the number of remapped and/or fixed parameters.
         * Ideal for use with optimiser, as only returns free parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Free parameters of the function
         */
        virtual unsigned int get_nr_of_free_params();

        /**
         * \brief Returns the squared distance between x1 and x2.
         *
         * \param x1 first group of data
         * \param x2 second group of data
         * \return Squared distance
         */
        static Eigen::MatrixXd squared_distance(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2
        );

        /**
         * \brief Fills the optimisation parameters.
         *
         * Only the free parameters are considered to fill the optimiser
         * parameters.
         *
         * \param optimiser_params to be filled
         */
        OptimiserParams get_optimiser_params();

        /**
         * \brief Fixes a param to a particular value. Not modifiable by the
         * optimiser.
         *
         * When a remapping exists, all of the remapped indexes to the input
         * index will get their values modified and fixed as a consequence.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param index Index of the parameter to be fixed (index in the range
         *              of total number of parameters), without considering
         *              previously remapped or clamped values.
         * \param value New value of the parameter.
         */
        virtual void fix_param(unsigned int index, double value);

        /**
         * \brief Sets a remapping for the array of indexes.
         *
         * Essentially it receives a vector of length equal to the total number
         * of parameters for the covariance function. The indexes should start
         * at cero and be integers.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * Example: Imagine a covariance function that has 4 parameters:
         *          [a b c d]
         *          A remapping of this parameters can be the vector:
         *          [0 0 1 1]
         *          After setting this remapping, the first and second hyper-
         *          parameters will be coupled togheter and act as one free
         *          parameter. Same will happen with the third and fourth
         *          hyperparameters.
         *          Not acceptable remapping vectors are:
         *          [2 0 0 0] or [0 0 0 2] (there should be consecutive values
         *          inside the remapping vector).
         *
         * \param remapping_vector Vector that remaps the indexes of the
         *                         hyperparameters of a covariance function.
         */
        virtual void set_remapping(std::vector<unsigned int> remapping_vector);

        /**
         * @brief Reset parameters of a covariance function.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         */
        virtual void reset_params();

        AbstractCovarianceFunction::ParamVec_t
                    get_hyper_params_from_free_params(ParamVec_t free_params);

    private:

        std::vector<unsigned int> get_unique_remapped_idx(
                std::vector<unsigned int> remapping_vector);

        void update_free_params();

    protected:

        /** All of the parameters of the covariance function
         *  including fixed and remapped ones.
         */
        ParamVec_t                      m_params;

        std::vector<bool>               m_idx_free;

        //! Remapping of hyperparameters (can be used to build isotropic
        //! covariance functions.
        std::vector<unsigned int> m_idx_remapped;

        std::vector<unsigned int> m_idx_fixed;

        //! Dimenstion of the data to which the covariance function is
        //! compatible with.
        unsigned int m_dim;
};

} /* gp */

#endif /* __GP_ABSTRACT_COVARIANCE_FUNCTION_H__ */
