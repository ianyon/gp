#include <iostream>
#include <math.h>

#include <gp/covariance_functions/matern3.h>
#include <gp/gp/error.hpp>

#include <iomanip>

namespace gp
{

Matern3::Matern3(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd Matern3::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    // Distance matrix
    Eigen::MatrixXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    // Fill covariance matrix
    return params[m_dim] *
           params[m_dim] *
           (1 + (3*dist_sig).array().sqrt()) *
           exp((-sqrt((3*dist_sig).array())));
}

Eigen::MatrixXd Matern3::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    double sigma = params[m_dim];

    // Init gradient matrix
    //Eigen::MatrixXd K_deriv(numx1,numx2);
    Eigen::MatrixXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    if (deriv_i < m_dim)
    {
        double sigma_i;
        sigma_i = param_inv(deriv_i);
         // Fill covariance matrix
        return sigma*sigma*(3*squared_distance(x1.col(deriv_i),
                                               x2.col(deriv_i)).array()*
                            (sigma_i*sigma_i*sigma_i)*
                            (-(3*dist_sig).array().sqrt()).exp());

    }
    else if (deriv_i == m_dim)
    {
        // Fill covariance matrix
        return 2*sigma*(1 + (3*dist_sig).array().sqrt())*
                (-(3*dist_sig).array().sqrt()).exp();
    }
    else
    {
        throw DimensionError("Wrong derivative index requested, no such parameter to derive with respect to.");
    }

}

unsigned int Matern3::get_nr_of_params()
{
    return m_dim + 1;
}

bool Matern3::has_derivatives()
{
    return true;
}

} /* gp */

