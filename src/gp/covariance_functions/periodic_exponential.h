#ifndef __GP_PERIODIC_EXPONENTIAL_H__
#define __GP_PERIODIC_EXPONENTIAL_H__

#include <gp/covariance_functions/abstract_covariance_function.h>

namespace gp
{

/**
 * \brief Periodic-Exponential covariance function.
 *
 * This class represents a Periodic-Exponential covariance function.
 *
 * \f[
 * k(x,x') = \sigma_f^2\cdot\exp{\left(-\frac{2\sin{\left(\frac{\pi\|x-x\|}{p}\right)}^2}{l^2}\right)}
 * \f]
 *
 * Hyperparameters are:
 * - \f$l\f$ : Smoothness of the function.
 * - \f$p\f$ : Period.
 * - \f$\sigma_f\f$: Signal variance.
 *
 */
class PeriodicExponential
        :public AbstractCovarianceFunction
{
    public:
        PeriodicExponential(unsigned int dim = 1);
        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \return covariance matrix computed from the given data
         */
        Eigen::MatrixXd do_cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Returns the number of hyperparameters for a given input
         * dimension
         *
         * \param dim the dimension of the input data
         * \return The number of hyperparameters
         **/
        unsigned int get_nr_of_params();

        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the covariance function has derivative
         * information
         */
        bool has_derivatives();
};

} /* gp */

#endif /* __GP_PERIODIC_EXPONENTIAL_H__ */
