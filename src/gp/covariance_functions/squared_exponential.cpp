#include <iostream>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/gp/error.hpp>

namespace gp
{

SquaredExponential::SquaredExponential(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd SquaredExponential::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params
)
{
    // Invert parameters
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    return params[m_dim] *
           params[m_dim] *
           exp((-0.5 *
           squared_distance(x1*param_inv.asDiagonal(),
                            x2*param_inv.asDiagonal())).array());
}

Eigen::MatrixXd SquaredExponential::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).
            inverse();

    Eigen::ArrayXXd K = squared_distance(x1*param_inv.asDiagonal(),
                                         x2*param_inv.asDiagonal());

    if (deriv_i < m_dim)
    {
        Eigen::ArrayXXd dist = squared_distance(x1.col(deriv_i),
                                                x2.col(deriv_i));
        return params[m_dim]*
               params[m_dim]/
               (params[deriv_i]*
                params[deriv_i]*
                params[deriv_i])*dist*exp(-.5*K);
    }
    else if (deriv_i == m_dim)
    {
        return 2*params[m_dim]*exp(-.5*K);
    }
    else
    {
        throw DerivativeError("Wrong derivative index requested, no such"
                              "parameter to derive with respect to.");
    }
}

unsigned int SquaredExponential::get_nr_of_params()
{
    return m_dim + 1;
}

bool SquaredExponential::has_derivatives()
{
    return true;
}

} /* gp */
