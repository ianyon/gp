#include <iostream>
#include <math.h>

#include <gp/covariance_functions/rational_quadratic.h>
#include <gp/gp/error.hpp>

namespace gp
{

RationalQuadratic::RationalQuadratic(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd RationalQuadratic::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();
    Eigen::ArrayXXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    // Calculate sigma squared
    double sigma_squared = params[m_dim] * params[m_dim];
    // Exponent parameter, alpha
    double alpha = params[m_dim+1];

    // Fill covariance matrix
    return sigma_squared*(1 + dist_sig/(2*alpha)).pow(-alpha);
}

Eigen::MatrixXd RationalQuadratic::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();
    Eigen::ArrayXXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    // Calculate sigma
    double sigma = params[m_dim];
    // Exponent parameter, alpha
    double alpha = params[m_dim+1];


    if (deriv_i < m_dim)
    {
        double sigma_i = param_inv(deriv_i);
        return sigma*sigma*sigma_i*sigma_i*sigma_i*
                squared_distance(x1.col(deriv_i),
                                 x2.col(deriv_i)).array()*
                ((1 + (dist_sig/(2*alpha))).pow(-alpha-1));
    }
    else if (deriv_i == m_dim)
    {
        return  2*sigma*(1 + dist_sig/(2*alpha)).pow(-alpha);
    }
    else if (deriv_i == m_dim + 1)
    {
        return sigma*sigma*(dist_sig/(2*alpha + dist_sig) -
                            log(1 + dist_sig/(2*alpha)))
                *(1 + dist_sig/(2*alpha)).pow(-alpha);
    }
    else
    {
        throw DerivativeError("Wrong derivative index requested, no such"
                              "parameter to derive with respect to.");
    }
}

unsigned int RationalQuadratic::get_nr_of_params()
{
    return m_dim + 2;
}

bool RationalQuadratic::has_derivatives()
{
    return true;
}

}/* gp */
