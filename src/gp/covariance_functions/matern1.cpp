#include <iostream>
#include <math.h>

#include <gp/covariance_functions/matern1.h>
#include <gp/gp/error.hpp>


namespace gp
{

Matern1::Matern1(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}
    
Eigen::MatrixXd Matern1::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    // Fill covariance matrix
    return params[m_dim] *
           params[m_dim] *
           exp(-sqrt((squared_distance(x1*param_inv.asDiagonal(),
                                       x2*param_inv.asDiagonal())).array()));
}

Eigen::MatrixXd Matern1::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    double sigma = params[m_dim];

    Eigen::MatrixXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    if (deriv_i < m_dim)
    {
        double sigma_i;
        sigma_i = param_inv(deriv_i);
        return sigma*sigma*(squared_distance(x1.col(deriv_i),
                                             x2.col(deriv_i)).array()
                            *sigma_i*sigma_i*sigma_i
                            /(dist_sig.array().sqrt())
                            *(-(dist_sig).array().sqrt()).exp());
    }
    else if (deriv_i == m_dim)
    {
        return 2*sigma*(-(dist_sig).array().sqrt()).exp();
    }
    else
    {
        throw DimensionError("Wrong derivative index requested, no such parameter to derive with respect to.");
    }
}

unsigned int Matern1::get_nr_of_params()
{
    return m_dim + 1;
}

bool Matern1::has_derivatives()
{
    return true;
}

} /* gp */
