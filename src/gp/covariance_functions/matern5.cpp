#include <iostream>
#include <math.h>

#include <gp/covariance_functions/matern5.h>
#include <gp/gp/error.hpp>


namespace gp
{

Matern5::Matern5(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd Matern5::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();

    // Distance matrix
    Eigen::ArrayXXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    // Fill covariance matrix
    return params[m_dim] *
           params[m_dim] *
           (1 + sqrt(5*dist_sig) + (5.0/3.0*dist_sig)) *
           exp(-sqrt(5*dist_sig));

}

Eigen::MatrixXd Matern5::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    const Eigen::VectorXd param_inv =
            (Eigen::Map<const Eigen::ArrayXd>(params.data(),m_dim)).inverse();
    Eigen::ArrayXXd dist_sig = squared_distance(x1*param_inv.asDiagonal(),
                                                x2*param_inv.asDiagonal());

    double sigma = params[m_dim];

    if (deriv_i < m_dim)
    {
        Eigen::ArrayXXd dist = squared_distance(x1.col(deriv_i),
                                                x2.col(deriv_i));
        double sigma_i = param_inv(deriv_i);

        // Fill covariance matrix
        return sigma*sigma*((sigma_i*sigma_i*sigma_i)*
                            (5/3.0 + 5.0/3.0*sqrt(5.0)*
                             sqrt(dist_sig))*dist*exp(-sqrt(5*dist_sig)));

    }
    else if (deriv_i == m_dim)
    {
        // Fill covariance matrix
        return 2*sigma*(1 + sqrt(5*dist_sig) + (5.0/3.0*dist_sig))*
                exp(-sqrt(5*dist_sig));
    }
    else
    {
        throw DimensionError("Wrong derivative index requested, no such parameter to derive with respect to.");
    }
}

unsigned int Matern5::get_nr_of_params()
{
    return m_dim + 1;
}


bool Matern5::has_derivatives()
{
    return true;
}

} /* gp */
