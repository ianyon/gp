#include <iostream>

#include <gp/covariance_functions/abstract_covariance_function.h>
#include <gp/gp/error.hpp>

namespace gp
{

AbstractCovarianceFunction::AbstractCovarianceFunction(unsigned int dim)
    :m_dim(dim)
{

}

AbstractCovarianceFunction::~AbstractCovarianceFunction()
{

}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params,
        bool                            free_params
)
{
    ParamVec_t full_params = params;
    // If the caller specifies that the parameters are only
    // the free parameters, then the covariance function
    // transforms this parameters to the full param dimensionality.
    if(free_params)
    {
       full_params = get_hyper_params_from_free_params(params);
    }
    dimensionality_check(x1);
    dimensionality_check(x2);
    dimensionality_check(full_params);
    return do_cov(x1,x2,full_params);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const& x1,
        Eigen::MatrixXd const& x2
)
{
    return cov(x1, x2, m_params, false);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const&  x1,
        ParamVec_t const&       params,
        bool                    free_params
)
{
    return cov(x1, x1, params, free_params);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const& x1
)
{
    return cov(x1, x1, m_params, false);
}


Eigen::MatrixXd AbstractCovarianceFunction::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i,
        bool                    free_params
)
{
    ParamVec_t full_params = params;
    // If the caller specifies that the parameters are only
    // the free parameters, then the covariance function
    // transforms this parameters to the full param dimensionality.
    if(free_params)
    {
       full_params = get_hyper_params_from_free_params(params);
    }
    dimensionality_check(x1);
    dimensionality_check(x2);
    dimensionality_check(full_params);
    return do_cov_deriv(x1,x2,full_params,deriv_i);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov_deriv(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        unsigned int const&             deriv_i
)
{
    return cov_deriv(x1,x2,m_params,deriv_i,false);
}

void AbstractCovarianceFunction::dimensionality_check(Eigen::MatrixXd const&  x)
{
    unsigned int input_dims = x.cols();
    if(input_dims != m_dim)
    {
        std::stringstream error;
        error << "Dimensionality mismach: "
              << "Covariance Funcion expected "
              << m_dim
              << " but data has dimensionality "
              << input_dims;
        throw DimensionError(error.str());
    }
}

void AbstractCovarianceFunction::dimensionality_check(ParamVec_t const& params)
{
    if(params.size() != get_nr_of_params())
    {
        std::stringstream error;
        error << "Wrong number of parameters provided: "
              << "Expected "
              << get_nr_of_params()
              << " but "
              << params.size()
              << " where given. ";
        throw ParameterError(error.str());
    }
}

OptimiserParams AbstractCovarianceFunction::get_optimiser_params()
{
    OptimiserParams params;
    params.set_parameter(OptimiserParams::NUMBER_OF_CONSTRAINTS,0);
    params.set_parameter(OptimiserParams::NUMBER_OF_UNKNOWNS,
                         get_nr_of_free_params());
    return params;
}

Eigen::MatrixXd AbstractCovarianceFunction::squared_distance(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2
)
{
    // Number of inputs
    unsigned int numx1 = x1.rows();
    unsigned int numx2 = x2.rows();

    // Calculate the mean and remove from data points to increase numerical
    // stability
    Eigen::MatrixXd mu = (double)numx2/(numx1*(numx1+numx2)) *
                                 x1.colwise().sum() +
                         (double)numx1/(numx2*(numx1+numx2)) *
                                 x2.colwise().sum();
    Eigen::MatrixXd x1c = x1 - mu.replicate(numx1,1);
    Eigen::MatrixXd x2c = x2 - mu.replicate(numx2,1);

    return ((x1c.array()*x1c.array())
            .rowwise().sum().replicate(1,numx2) +
           ((x2c.array()*x2c.array()).rowwise()
            .sum()).transpose().replicate(numx1,1) -
            2.0*(x1c*x2c.transpose()).array()).array()
            .max(Eigen::ArrayXXd::Zero(numx1,numx2));
}

void AbstractCovarianceFunction::fix_param(unsigned int index, double value)
{
    if(index > get_nr_of_params())
    {
        throw ParameterError("AbstractCovarianceFunction: "
                             "Index larger than number of params.");
    }

    m_idx_fixed.push_back(index);
    m_params[index] = value;

    //Go through the remapping array and fix all associated values
    unsigned int remapped_index = m_idx_remapped[index];
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        if(m_idx_remapped[i] == remapped_index)
        {
            m_params[i] = value;
        }
    }

    update_free_params();
}

void AbstractCovarianceFunction::set_remapping(
        std::vector<unsigned int> remapping_vector
)
{
    if(remapping_vector.size() != get_nr_of_params())
    {
        throw ParameterError("AbstractCovarianceFunction: "
                             "Remapping vector of the wrong size.");
    }

    std::vector<unsigned int> unique =
            get_unique_remapped_idx(remapping_vector);

    unsigned int min;
    unsigned int max;
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        unsigned int remapped_index = remapping_vector[i];
        if(i == 0)
        {
            min = remapped_index;
            max = remapped_index;
        }
        else
        {
            if(remapped_index < min)
            {
                min = remapped_index;
            }
            if(remapped_index > max)
            {
                max = remapped_index;
            }
        }
    }
    if(min != 0)
    {
        throw ParameterError("AbstractCovarianceFunction: "
                             "Min of remapping vector has to be zero");
    }
    if(max != unique.size()-1)
    {
        throw ParameterError("AbstractCovarianceFunction: "
                             "Remapping must contain consecutive values");
    }

    m_idx_remapped = remapping_vector;

    //Check for all fixed params and copy their values
    for(unsigned int i = 0; i < m_idx_fixed.size(); i++)
    {
        double value = m_params[m_idx_fixed[i]];
        unsigned int remapped_idx = m_idx_remapped[m_idx_fixed[i]];
        for(unsigned int j = 0; j < get_nr_of_params(); j++)
        {
            if(m_idx_remapped[j] == remapped_idx)
            {
                m_params[j] = value;
            }
        }
    }

    update_free_params();
}

void AbstractCovarianceFunction::update_free_params()
{
    //Assume initially all of the params are free and then only fix the ones
    //that are fixed directly or indirectly.
    m_idx_free.assign(get_nr_of_params(),true);

    std::vector<unsigned int> unique_idx =
            get_unique_remapped_idx(m_idx_remapped);

    // For each unique_idx find all of their appearence and check if any of
    // them is not free
    for(unsigned int i = 0; i < unique_idx.size(); i++)
    {
        unsigned int current_unique_id = unique_idx[i];
        bool fixed_params = false;
        // Find the appearences of this id in the param vector
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == current_unique_id)
            {
                appearences.push_back(j);
            }
        }
        if(appearences.size() < 1)
        {
            throw ParameterError("AbstractCovarianceFunction: "
                                 "Error inside remapped index array.");
        }
        //Check if any of the appearences are fixed
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            for(unsigned int k = 0; k < m_idx_fixed.size(); k++)
            {
                if(m_idx_fixed[k] == appearences[j])
                {
                    fixed_params = true;
                }
            }
        }
        //All the appearences are set to not free
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            m_idx_free[appearences[j]] = false;
        }
        if(!fixed_params)
        {
            //If the current id is not fixed then only the first one is set to
            //free
            m_idx_free[appearences[0]] = true;
        }
    }
}

AbstractCovarianceFunction::ParamVec_t
    AbstractCovarianceFunction::get_hyper_params_from_free_params(
                                            ParamVec_t free_params)
{
    ParamVec_t full_params = m_params;

    std::vector<unsigned int> unique_idx =
                get_unique_remapped_idx(m_idx_remapped);

    for(unsigned int i = 0; i < unique_idx.size(); i++)
    {
        unsigned int current_unique_id = unique_idx[i];
        bool add_from_free_param = true;
        // Find the appearences of this id in the param vector
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == current_unique_id)
            {
                appearences.push_back(j);
            }
        }
        // Check if none of the appearences has been fixed
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            for(unsigned int k = 0; k < m_idx_fixed.size(); k++)
            {
                if(m_idx_fixed[k] == appearences[j])
                {
                    add_from_free_param = false;
                }
            }
        }
        // This group of parameters hasn't been fixed.
        if(add_from_free_param)
        {
            for(unsigned int j = 0; j < appearences.size(); j++)
            {
                full_params[appearences[j]] = free_params[i];
            }
        }
    }

    return full_params;
}

std::vector<unsigned int> AbstractCovarianceFunction::get_unique_remapped_idx(
        std::vector<unsigned int> remapping_vector)
{
    // Find the number of different indexes in the remaped array
    std::vector<unsigned int> unique_idx;
    for(unsigned int i = 0; i < remapping_vector.size(); i++)
    {
        bool is_repeated = false;
        for(unsigned int j = 0; j < unique_idx.size(); j++)
        {
            if(unique_idx[j] == remapping_vector[i])
            {
                is_repeated = true;
                break;
            }
        }
        if(!is_repeated)
        {
            unique_idx.push_back(remapping_vector[i]);
        }
    }
    return unique_idx;
}


void AbstractCovarianceFunction::set_hyper_params(
        ParamVec_t const& params,
        bool force
)
{
    dimensionality_check(params);

    if(force)
    {
        reset_params();
        m_params = params;
    }
    else
    {
        //Check if they agree with the remapped parameters.
        std::vector<unsigned int> unique_idx =
                get_unique_remapped_idx(m_idx_remapped);


        //No remapping has been set if unique_idx has the same dimension
        //as the number of params.
        if(unique_idx.size() < get_nr_of_params())
        {
            // For each unique_idx find all of their appearence and check if
            // they are all the same.
            for(unsigned int i = 0; i < unique_idx.size(); i++)
            {
                unsigned int current_unique_id = unique_idx[i];
                // Find the appearences of this id in the param vector
                std::vector<unsigned int> appearences;
                for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
                {
                    if(m_idx_remapped[j] == current_unique_id)
                    {
                        appearences.push_back(j);
                    }
                }
                if(appearences.size() < 1)
                {
                    throw ParameterError("AbstractCovarianceFunction: "
                                         "Error inside remapped index array.");
                }
                double value = params[appearences[0]];
                for(unsigned int j = 0; j < appearences.size(); j++)
                {
                    if(params[appearences[j]] != value)
                    {
                        throw ParameterError("AbstractCovarianceFunction :"
                                             "params not compatible with "
                                             "current remapping");
                    }
                }
            }
        }
        else
        {
            m_params = params;
        }
    }
}

void AbstractCovarianceFunction::set_free_hyper_params(ParamVec_t const& params)
{
    if(params.size() != get_nr_of_free_params())
    {
        throw ParameterError("AbstractCovarianceFunction: "
                             "Number of free params does not match.");
    }

    unsigned int idx = 0;
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        //Find all ocurrances of i inside the remapping index
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == i)
            {
                appearences.push_back(j);
            }
        }
        bool at_least_one_free = false;
        //Check if any of those appearences is free
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            if(m_idx_free[appearences[j]])
            {
                at_least_one_free = true;
                break;
            }
        }
        if(at_least_one_free)
        {
            //Replace all repetitions of that param depending on the
            //remapping
            for(unsigned int j = 0; j < appearences.size(); j++)
            {
                m_params[appearences[j]] = params[idx];
            }
            idx++;
        }
    }

}

AbstractCovarianceFunction::ParamVec_t
    AbstractCovarianceFunction::get_hyper_params() const
{
    return m_params;
}

AbstractCovarianceFunction::ParamVec_t
    AbstractCovarianceFunction::get_free_hyper_params()
{
    ParamVec_t free_params;
    free_params.clear();
    unsigned int n_pars = get_nr_of_params();

    for(unsigned int i = 0; i < n_pars; i++)
    {
        if(m_idx_free[i])
        {
            free_params.push_back(m_params[i]);
        }
    }

    return free_params;
}

void AbstractCovarianceFunction::reset_params()
{
    m_params.clear();
    //Initialisation of param vector, all in ones.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_params.push_back(1);
    }

    m_idx_remapped.clear();
    //Initialisation of remapping vector, in consecutive order.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_idx_remapped.push_back(i);
    }

    m_idx_free.clear();
    //Initialisation of remapping vector, in consecutive order.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_idx_free.push_back(true);
    }

    m_idx_fixed.clear();
}

unsigned int AbstractCovarianceFunction::get_nr_of_free_params()
{

    return get_free_hyper_params().size();
}

} /* gp */
