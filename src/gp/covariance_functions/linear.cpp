#include <iostream>
#include <math.h>

#include <gp/covariance_functions/linear.h>
#include <gp/gp/error.hpp>


namespace gp
{

// FIXME:
// - this linear thing is completely different between this implementation,
//   Fabio's version and the equation in the GP book
//   * Fabio: sigma^-2, bias^2+(sigma*x1)'*x2
//   * Here : sigma^2, bias^2+(x1*diag(sigma))*x2'
//   * Book : sigma^2, sigma*x1*x2'

Linear::Linear(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd Linear::do_cov(
    Eigen::MatrixXd const&              x1,
    Eigen::MatrixXd const&              x2,
    Linear::ParamVec_t const&           params
)
{
    // Compute parameters
    const Eigen::VectorXd params_square =
        (Eigen::Map<const Eigen::ArrayXd>(params.data(), m_dim)).square();
    double bias = params.back() * params.back();

    return bias + (x1 * params_square.asDiagonal() * x2.transpose()).array();
}

Eigen::MatrixXd Linear::do_cov_deriv(
    Eigen::MatrixXd const&              x1,
    Eigen::MatrixXd const&              x2,
    Linear::ParamVec_t const&           params,
    unsigned int const&                 deriv_i
)
{
    // Dimensions on input
    unsigned int dim = x1.cols();
    unsigned int numx1 = x1.rows();
    unsigned int numx2 = x2.rows();

    // Calculate sigma squared
    double sigma = params[dim];
    double sigma_i = params[deriv_i];

    if (deriv_i < dim)
    {
        return 2*sigma_i*x1.col(deriv_i)*x2.col(deriv_i).transpose();

    }
    else if (deriv_i == dim)
    {
        return 2*sigma*Eigen::MatrixXd::Ones(numx1,numx2);
    }
    else
    {
        throw DimensionError("Wrong derivative index requested, no such parameter to derive with respect to.");
    }
}

unsigned int Linear::get_nr_of_params()
{
    return m_dim + 1;
}

bool Linear::has_derivatives()
{
    return true;
}

} /* gp */
