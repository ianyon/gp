#include <iostream>
#include <limits>

#include <boost/bind.hpp>

#include <gtest/gtest.h>

#include <gp/util/clenshaw_curtis_quadrature.h>


using namespace std;
using namespace gp;


class MyClass
{
    public:
        MyClass() : k(1), m(2) {}
        double f(double x) { return (k * x) + m; }

    private:
        double k;
        double m;

};

double f(double x, double l)
{
    return ((x*x*x) - (3*x*x) - (4*x) + 1) * exp(-pow(x / l, 2));
}

double uniform(double x, double a, double b)
{
    if(x < a || x > b)
    {
        return 0;
    }
    else
    {
        return 1.0 / (b - a);
    }
}

TEST(ClenshawCurtisQuadrature, Simple)
{
    double eps = 0.0001;
    ClenshawCurtisQuadrature CCQ;

    double inf = numeric_limits<double>::infinity();

    MyClass myclass;

    ASSERT_NEAR(CCQ.integrate(0.0, 1.0, boost::bind(&MyClass::f, myclass, _1)), 2.5, eps);
    ASSERT_NEAR(CCQ.integrate(-inf, inf, boost::bind(&f, _1, 3)), -66.5056, eps);
    ASSERT_NEAR(CCQ.integrate(-5, 5, boost::bind(&uniform, _1, -5, 5)), 1, eps);
    ASSERT_NEAR(CCQ.integrate(0, 5, boost::bind(&uniform, _1, -5, 5)), 0.5, eps);
    CCQ.set_order(1000);
    ASSERT_NEAR(CCQ.integrate(-10, 10, boost::bind(&uniform, _1, -5, 5)), 1, 0.01);
}
