#ifndef __GP_TEST_DATA_H__
#define __GP_TEST_DATA_H__


#include <Eigen/Core>


namespace gp
{

Eigen::MatrixXd data_4_1()
{
    //Four one dimensional data points
    Eigen::MatrixXd data(4, 1);
    data << -2.1775,
            -0.9235,
             0.7502,
            -5.8868;
    return data;
}

Eigen::MatrixXd data_5_1()
{
    //Five one dimensional data points
    Eigen::MatrixXd data(5, 1);
    data << -2.7995,
             4.2504,
             2.4582,
             6.1426,
            -4.0911;
    return data;
}

Eigen::MatrixXd data_4_2()
{
    //Four two dimensional data points
    Eigen::MatrixXd data(4, 2);
    data << -2.142,  1.543,
             5.186, -1.243,
             1.475, -3.485,
            -12.348, 2.148;
    return data;
}

Eigen::MatrixXd data_6_2()
{
    //Six two dimensional data points
    Eigen::MatrixXd data(6, 2);
    data <<  3.214, -1.025,
             5.126,  5.147,
            -1.025, -4.123,
             1.257, -5.147,
             2.354, -2.746,
             1.288,  3.146;
    return data;
}
    
} /* gp */

#endif /* __GP_TEST_DATA_H__ */
