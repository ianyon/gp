#include <gtest/gtest.h>

#include <gp/covariance_functions/periodic_exponential.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>


using namespace gp;


TEST(PeriodicExponential, OneDim)
{
    PeriodicExponential covfun(1);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(1);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 4);
    result <<
              2.250000000000000e+00,     2.895781776667804e-01,     1.836918863038875e+00,     1.836056877269782e-01,
              2.895781776667804e-01,     2.250000000000000e+00,     1.210470379349692e-01,     2.133987569462818e+00,
              1.836918863038875e+00,     1.210470379349692e-01,     2.250000000000000e+00,     8.268333288004365e-02,
              1.836056877269782e-01,     2.133987569462818e+00,     8.268333288004365e-02,     2.250000000000000e+00;

    compare_matrices(covfun.cov(data_4_1()), result);
}

TEST(PeriodicExponential, TwoDim)
{
    PeriodicExponential covfun(2);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(1);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 6);
    result <<
                1.953474591713009e+00,     1.393404901366704e+00,     4.166392160023109e-01,     4.123560826391665e-02,     4.788669585735953e-01,    4.774121031024285e-01,
                2.227430704909817e+00,     6.506619304317149e-02,     9.492237613613121e-01,     4.372022688746027e-02,     5.248363836409103e-01,    1.198017397776347e+00,
                2.235963255994717e+00,     7.579853832476698e-02,     5.281674176175527e-02,     1.245333816413456e-01,     1.002977793765514e+00,    8.009837804612076e-02,
                1.333114786675932e+00,     2.354770084747534e-01,     1.986728453331205e+00,     4.801193454540403e-02,     4.124828483467928e-02,    1.194047212787981e-01;

    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(PeriodicExponential, Derivative)
{
    test_derivative<PeriodicExponential>();
}
