#include <gtest/gtest.h>

#include <gp/covariance_functions/rational_quadratic.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(RationalQuadratic, OneDim)
{
    RationalQuadratic covfun(1);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(1.5);
    params.push_back(1.1);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
             2.25,  0.509384,   0.10245,  0.062433,
         0.509384,      2.25,  0.308213, 0.0335226,
          0.10245,  0.308213,      2.25, 0.0178779,
         0.062433, 0.0335226, 0.0178779,      2.25;
    compare_matrices(covfun.cov(data_4_1()), result); 
}

TEST(RationalQuadratic, TwoDim)
{
    RationalQuadratic covfun(2);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(0.75);
    params.push_back(1.5);
    params.push_back(1.1);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
         0.0256106,  0.0131056,  0.0549648,  0.0257629,  0.0287698,  0.0667944,
          0.225073,  0.0465077,  0.0187003,  0.0374691,  0.0971331,  0.0348577,
          0.154905,  0.0168778,   0.137492,   0.601372,   0.712042,  0.0428961,
        0.00271786, 0.00211961, 0.00484188, 0.00326392, 0.00298009, 0.00369595;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(RationalQuadratic, Derivatives)
{
    test_derivative<RationalQuadratic>();

    SUCCEED();
}
