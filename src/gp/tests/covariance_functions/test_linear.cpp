#include <gtest/gtest.h>

#include <gp/covariance_functions/linear.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(Linear, OneDim)
{
    Linear covfun(1);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 4);
    result <<
        3.43538, 2.75273, 1.84161, 5.45463,
        2.75273, 2.46321, 2.0768,  3.60911,
        1.84161, 2.0768,  2.3907,  1.14593,
        5.45463, 3.60911, 1.14593, 10.9136;
    compare_matrices(covfun.cov(data_4_1()), result);
}

TEST(Linear, TwoDim)
{
    Linear covfun(2);

    std::vector<double> hyper_params;
    hyper_params.clear();
    hyper_params.push_back(0.5);
    hyper_params.push_back(0.75);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 6);
    result <<
        -0.360733,  3.9723,  -0.779619, -2.8904,   -1.39392,  4.29081,
         7.13362,   5.29714,  3.80384,   7.47842,   7.22193,  1.72025,
         5.44448,  -5.94952,  9.9544,    12.8032,   8.50106, -3.44219,
        -8.91007,  -7.3551,   0.43256,  -7.84922,  -8.33465,  2.0751;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(Linear, Derivatives)
{
    test_derivative<Linear>();
}
