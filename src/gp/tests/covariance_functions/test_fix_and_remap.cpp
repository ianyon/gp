#include <gtest/gtest.h>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/tests/test_helpers.hpp>

using namespace gp;

TEST(SquaredExponential, FixParam)
{
    //Squared exponential with 5 dimensions,
    //translates into 6 parameters (5 lengthscales and 1 signal variance)
    SquaredExponential covfun(5);

    //To test we will fix the 3rd and 4th lengthscale to 45 and 50
    //respectively
    covfun.fix_param(2,45);
    covfun.fix_param(3,50);

    //now we should have 4 free parameters, we will set them to
    // 100 , 200 , 300, 400 respectively
    std::vector<double> free_hyper_params;
    free_hyper_params.push_back(100);
    free_hyper_params.push_back(200);
    free_hyper_params.push_back(300);
    free_hyper_params.push_back(400);

    covfun.set_free_hyper_params(free_hyper_params);

    //Now we test if the free param array is correct

    //The expected array is:
    std::vector<double> expected_free_param_array;
    expected_free_param_array.push_back(100);
    expected_free_param_array.push_back(200);
    expected_free_param_array.push_back(300);
    expected_free_param_array.push_back(400);

    if(covfun.get_free_hyper_params() != expected_free_param_array)
    {
        FAIL();
    }

    //Now we test if the full param array is correct
    //The expected array is:
    std::vector<double> expected_full_param_array;
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(200);
    expected_full_param_array.push_back(45);
    expected_full_param_array.push_back(50);
    expected_full_param_array.push_back(300);
    expected_full_param_array.push_back(400);

    if(covfun.get_hyper_params() != expected_full_param_array)
    {
        FAIL();
    }
}

TEST(SquaredExponential, RemapParams)
{
    //Squared exponential with 5 dimensions,
    //translates into 6 parameters (5 lengthscales and 1 signal variance)
    SquaredExponential covfun(5);

    //We will make two groups of parameters,
    //Lengthscales 0,1 and 4 will be grouped and lengthscales 2 and 3
    //will be grouped as well.
    std::vector<unsigned int> remapping;
    remapping.push_back(0);
    remapping.push_back(0);
    remapping.push_back(1);
    remapping.push_back(1);
    remapping.push_back(0);
    remapping.push_back(2);
    covfun.set_remapping(remapping);

    //now we should have 3 free parameters, we will set them to
    // 10 , 20 , 30 respectively
    std::vector<double> free_hyper_params;
    free_hyper_params.push_back(10);
    free_hyper_params.push_back(20);
    free_hyper_params.push_back(30);

    covfun.set_free_hyper_params(free_hyper_params);

    //Now we test if the free param array is correct

    //The expected array is:
    std::vector<double> expected_free_param_array;
    expected_free_param_array.push_back(10);
    expected_free_param_array.push_back(20);
    expected_free_param_array.push_back(30);

    if(covfun.get_free_hyper_params() != expected_free_param_array)
    {
        FAIL();
    }

    //Now we test if the full param array is correct
    //The expected array is:
    std::vector<double> expected_full_param_array;
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(20);
    expected_full_param_array.push_back(20);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(30);

    if(covfun.get_hyper_params() != expected_full_param_array)
    {
        FAIL();
    }
}

TEST(SquaredExponential, FixAndRemap)
{
    //Squared exponential with 5 dimensions,
    //translates into 6 parameters (5 lengthscales and 1 signal variance)
    SquaredExponential covfun(5);

    //We will make two groups of parameters,
    //Lengthscales 0,1 and 4 will be grouped and lengthscales 2 and 3
    //will be grouped as well.
    std::vector<unsigned int> remapping;
    remapping.push_back(0);
    remapping.push_back(0);
    remapping.push_back(1);
    remapping.push_back(1);
    remapping.push_back(0);
    remapping.push_back(2);
    covfun.set_remapping(remapping);

    //Now we will fix the second parameter (index 1) to 100.
    covfun.fix_param(1,100);

    //now we should have 2 free parameters, we will set them to
    // 10 , 20
    std::vector<double> free_hyper_params;
    free_hyper_params.push_back(10);
    free_hyper_params.push_back(20);

    covfun.set_free_hyper_params(free_hyper_params);

    //Now we test if the free param array is correct

    //The expected array is:
    std::vector<double> expected_free_param_array;
    expected_free_param_array.push_back(10);
    expected_free_param_array.push_back(20);

    if(covfun.get_free_hyper_params() != expected_free_param_array)
    {
        FAIL();
    }

    //Now we test if the full param array is correct
    //The expected array is:
    std::vector<double> expected_full_param_array;
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(20);

    if(covfun.get_hyper_params() != expected_full_param_array)
    {
        FAIL();
    }
}

TEST(SquaredExponential, RemapAndFix)
{
    // Same as Fix and Remap but the other way around.

    //Squared exponential with 5 dimensions,
    //translates into 6 parameters (5 lengthscales and 1 signal variance)
    SquaredExponential covfun(5);

    //Now we will fix the second parameter (index 1) to 100.
    covfun.fix_param(1,100);

    //We will make two groups of parameters,
    //Lengthscales 0,1 and 4 will be grouped and lengthscales 2 and 3
    //will be grouped as well.
    std::vector<unsigned int> remapping;
    remapping.push_back(0);
    remapping.push_back(0);
    remapping.push_back(1);
    remapping.push_back(1);
    remapping.push_back(0);
    remapping.push_back(2);
    covfun.set_remapping(remapping);

    //now we should have 2 free parameters, we will set them to
    // 10 , 20
    std::vector<double> free_hyper_params;
    free_hyper_params.push_back(10);
    free_hyper_params.push_back(20);

    covfun.set_free_hyper_params(free_hyper_params);

    //Now we test if the free param array is correct

    //The expected array is:
    std::vector<double> expected_free_param_array;
    expected_free_param_array.push_back(10);
    expected_free_param_array.push_back(20);

    if(covfun.get_free_hyper_params() != expected_free_param_array)
    {
        FAIL();
    }

    //Now we test if the full param array is correct
    //The expected array is:
    std::vector<double> expected_full_param_array;
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(10);
    expected_full_param_array.push_back(100);
    expected_full_param_array.push_back(20);

    if(covfun.get_hyper_params() != expected_full_param_array)
    {
        FAIL();
    }
}
