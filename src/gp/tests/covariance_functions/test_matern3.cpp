#include <gtest/gtest.h>

#include <gp/covariance_functions/matern3.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(Matern3, OneDim)
{
    Matern3 covfun(1);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
               2.25,    0.156128, 0.000987621, 8.18829e-05,
           0.156128,        2.25,   0.0464059, 1.39671e-06,
        0.000987621,   0.0464059,        2.25, 5.58812e-09,
        8.18829e-05, 1.39671e-06, 5.58812e-09,        2.25;
    compare_matrices(covfun.cov(data_4_1()), result);
}

TEST(Matern3, TwoDim)
{
    Matern3 covfun(2);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(0.75);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
        1.59983e-07, 1.88596e-10, 3.90703e-05, 1.68306e-07, 4.21986e-07, 0.000119086,
           0.018717, 1.38085e-05, 8.77536e-09, 3.16724e-06, 0.000776131, 1.87071e-06,
         0.00529207,  3.0925e-09,  0.00336965,    0.221079,     0.30799, 8.10068e-06,
        2.93985e-22, 4.83008e-25, 6.68259e-17,  2.1079e-20, 2.63909e-21, 3.14132e-19;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(Matern3, Derivatives)
{
    test_derivative<Matern3>();
}
