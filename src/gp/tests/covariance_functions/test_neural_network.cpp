#include <gtest/gtest.h>

#include <gp/covariance_functions/neural_network.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(NeuralNetwork, OneDim)
{
    NeuralNetwork covfun(1);
    std::vector<double> params;
    params.push_back(-19.33);
    params.push_back(9.89);
    params.push_back(-11.77);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
          6.07229,    4.1767,   1.56716,   10.9976,
           4.1767,   3.38082,   2.26414,   6.17567,
          1.56716,   2.26414,   3.17574, -0.397942,
          10.9976,   6.17567, -0.397942,   23.7739;
    compare_matrices(covfun.cov(data_4_1()), result); 
}

TEST(NeuralNetwork, TwoDim)
{
    NeuralNetwork covfun(2);
    std::vector<double> params;
    params.push_back(-19.33);
    params.push_back(12.19);
    params.push_back(9.89);
    params.push_back(-11.77);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
        -4.82001,  7.43321, -6.36856, -11.4507, -7.91176,  8.79782,
         15.4894,  7.91961,  6.92901,   15.323,  15.6539, 0.421122,
         11.4087, -18.6201,  23.3811,  29.4777,  19.8835,  -13.765,
        -21.4272, -13.8599,  -2.7828, -18.1111, -20.1694,  2.44974;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(NeuralNetwork, Derivatives)
{
    test_derivative<NeuralNetwork>();
}
