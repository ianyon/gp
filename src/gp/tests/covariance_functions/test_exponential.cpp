#include <gtest/gtest.h>

#include <gp/covariance_functions/exponential.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>


using namespace gp;


TEST(Exponential, OneDim)
{
    Exponential covfun(1);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 4);
    result <<
        2.250000000000000,   0.183219611319593,   0.006444876636194,   0.001349974218956,
        0.183219611319593,   2.250000000000000,   0.079145307246296,   0.000109929667417,
        0.006444876636194,   0.079145307246296,   2.250000000000000,   0.000003866852135,
        0.001349974218956,   0.000109929667417,   0.000003866852135,   2.250000000000000;

    compare_matrices(covfun.cov(data_4_1()), result);
}

TEST(Exponential, TwoDim)
{
    Exponential covfun(2);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(0.75);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 6);
    result <<
                2.938606625445219e-05,     5.049749270390426e-07,     8.526815631477317e-04,     3.030485594149891e-05,     5.298581745146594e-05,     1.704781841426120e-03,
                4.312009626351031e-02,     4.483596702362181e-04,     5.075642307560518e-06,     1.814081263726777e-04,     5.532362408989054e-03,     1.314254740237510e-04,
                1.887819024833785e-02,     2.707922865293379e-06,     1.410935876061002e-02,     2.351446948921244e-01,     2.998800602172356e-01,     3.228541363676592e-04,
                5.139221361330786e-14,     1.189688490084894e-15,     7.382293188665033e-11,     6.349053827212116e-13,     1.868595931622229e-13,     3.118984325716001e-12;

    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(Exponential, Derivative)
{
    test_derivative<Exponential>();
}
