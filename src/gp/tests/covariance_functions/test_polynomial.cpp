#include <gtest/gtest.h>

#include <gp/covariance_functions/polynomial.hpp>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(Polynomial, OneDim)
{
    Polynomial<2> covfun(1);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
        11.8018, 7.57752, 3.39153,  29.753,
        7.57752, 6.06742, 4.31309, 13.0257,
        3.39153, 4.31309, 5.71545, 1.31316,
         29.753, 13.0257, 1.31316, 119.107;
    compare_matrices(covfun.cov(data_4_1()), result, 0.001); 
}

TEST(Polynomial, TwoDim)
{
    Polynomial<2> covfun(2);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(0.75);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
        0.130128,  15.7792, 0.607805,   8.3544,  1.94302,   18.411,
         50.8885,  28.0597,  14.4692,  55.9267,  52.1563,  2.95925,
         29.6424,  35.3967,  99.0901,  163.923,  72.2679,  11.8487,
         79.3894,  54.0975, 0.187108,  61.6103,  69.4664,  4.30603;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result, 0.001);
}

TEST(Polynomial, Derivatives)
{
    test_derivative<Polynomial<2> >();
}
