#include <gtest/gtest.h>

#include <gp/gp/gp_simple.h>
#include <gp/covariance_functions/summing_kernel.h>
#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/neural_network.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/linear.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(SummingKernel, OneDim)
{
    boost::shared_ptr<SquaredExponential> squared_exp(new SquaredExponential);
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3);
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(3.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5);
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(3.0);
    matern5->set_hyper_params(m5_hyper_params);

    SummingKernel summing_kernel;

    summing_kernel.add_cov_func(squared_exp);
    summing_kernel.add_cov_func(matern3);
    summing_kernel.add_cov_func(matern5);


    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 1) * 5.0;
    compare_matrices(
            summing_kernel.cov(data),
            (matern5->cov(data) + squared_exp->cov(data) + matern3->cov(data))
    );
}

TEST(SummingKernel, HighDim)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(3.0);
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_hyper_params(m5_hyper_params);

    SummingKernel summing_kernel(3);

    summing_kernel.add_cov_func(squared_exp);
    summing_kernel.add_cov_func(matern3);
    summing_kernel.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            summing_kernel.cov(data),
            (matern5->cov(data) + squared_exp->cov(data) + matern3->cov(data))
    );
}

TEST(SummingKernel, ClampAndRemap)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));

    squared_exp->fix_param(0,10);

    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_free_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));

    std::vector<unsigned int> matern3_remapping;
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(1);
    matern3->set_remapping(matern3_remapping);

    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_free_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_free_hyper_params(m5_hyper_params);

    SummingKernel summing_kernel(3);

    summing_kernel.add_cov_func(squared_exp);
    summing_kernel.add_cov_func(matern3);
    summing_kernel.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            summing_kernel.cov(data),
            (matern5->cov(data) + squared_exp->cov(data) + matern3->cov(data))
    );
}
