#include <iostream>
#include <fstream>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/summing_kernel.h>
#include <gp/gp/gp_simple.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/optimisers/nlopt_optimiser.h>


using namespace gp;
using namespace std;

int main(int argc, char *argv[])
{

    // Create a new GP
    GaussianProcess gp(
        boost::shared_ptr<SummingKernel>(new SummingKernel()),
        boost::shared_ptr<ZeroMean>(new ZeroMean())
    );

    boost::shared_ptr<SquaredExponential> squared_exp(new SquaredExponential);
    boost::shared_ptr<Matern3> matern3(new Matern3);
    boost::shared_ptr<Matern5> matern5(new Matern5);
    (boost::dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
                                                    add_cov_func(squared_exp);
    (boost::dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
                                                    add_cov_func(matern3);
    (boost::dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
                                                    add_cov_func(matern5);
    // Set sensible initial parameters
    std::vector<double> hyper_params;
    hyper_params.push_back(10.0);
    hyper_params.push_back(3.0);
    hyper_params.push_back(1.0);
    hyper_params.push_back(3.0);
    hyper_params.push_back(5.0);
    hyper_params.push_back(3.0);
    gp.set_hyper_params(hyper_params);
    gp.set_noise(0.4);

    // Create training data and labels (from the Rasmusen example)
    Eigen::MatrixXd X(20, 1);
    Eigen::ArrayXd y(20);
    X << -2.1775, -0.9235,  0.7502, -5.8868, -2.7995,
          4.2504,  2.4582,  6.1426, -4.0911, -6.3481,
          1.0004, -4.7591,  0.4715,  4.8933,  4.3248,
         -3.7461, -7.3005,  5.8177,  2.3851, -6.3772;
    y <<  1.4121,  1.6936, -0.7444,  0.2493,  0.3978,
         -1.2755, -2.2210, -0.8452, -1.2232,  0.0105,
         -1.0258, -0.8207, -0.1462, -1.5637, -1.0980,
         -1.1721, -1.7554, -1.0712, -2.6937, -0.0329;

	cout<<"X.size: \n"<<X.rows()<<", "<<X.cols()<<endl;
        cout<<"y.size: \n"<<y.rows()<<", "<<y.cols()<<endl;

    // Feed the data to the GP
    gp.set_training_data(X,y);

    // Optimise the hyper parameters
    NLoptOptimiser optimiser;
    optimiser.optimise(gp);

    // Create a set of points to perform predictions for
    Eigen::MatrixXd test_points(201,1);
    test_points.col(0) = Eigen::ArrayXd::LinSpaced(201, -7.5f, 7.5f);

    // Get the predictions from the GP
    gp.set_query_points(test_points);
    GPResult output = gp.inference();

    // Write data and predictions to the harddisk for plotting
    ofstream data("/tmp/data.dat");
    data << X.transpose() << endl;
    data << y.transpose() << endl;
    ofstream pred("/tmp/pred.dat");
    pred << test_points.col(0).transpose() << endl;
    pred << output.mean.transpose() << endl;
    pred << output.variance.transpose() << endl;

    return 0;
}
