#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <random>
#include <ctime>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/summing_kernel.h>
#include <gp/gp/gp_simple.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/optimisers/nlopt_optimiser.h>


using namespace gp;
using namespace std;
using namespace Eigen;
using namespace boost;

struct MatrixData
{
	vector<double> data;
	unsigned int height;
	unsigned int width;
};

/*Funcion que crea el array un array de doubles a partir de un archivo que contiene el tamanno y de la matriz y luego los elementos separados por espacios
 * */
void SetMatrixDataFromFile(const string &path, MatrixData &result)
{
    ifstream file(path.c_str());
    istream_iterator <double> it( file );
    result.height = *it++;
    result.width = *it++;
    //cout << path.c_str() << "= h:" << result.height << " w:" << result.width << endl;
    
    copy(
            it,
            istream_iterator <double> (),
            back_inserter( result.data )
            );
    if (result.data.size() != result.height * result.width)
        cout<<"Warning: wrong number of elements in matrix file "<<path<<" "<<result.data.size()<<"!=["<<result.height <<", "<< result.width<<"]"<<endl;
}

void ReadMatrixFromFile(const string &path, MatrixXd &m)
{
	MatrixData md;
    SetMatrixDataFromFile(path, md);
	m = Map<MatrixXd>(&md.data[0],md.height,md.width);
}

void ReadVectorFromFile(const string &path, ArrayXd &v)
{
	MatrixData md;
	SetMatrixDataFromFile(path, md);
	v = Map<ArrayXd>(&md.data[0],md.width);
}

void WriteVectorToFile(const vector<double> &v, const string &fileName)
{
	ofstream f(fileName);
	f<<v.size()<<"\t1\n";
    copy(
			v.begin(),
			v.end(),
			ostream_iterator<double>( f , "\n")
            );
	f.close();
}

void GetSqrExpHyperParamsLimits(vector<double> &minHP, vector<double> &maxHP, const MatrixXd &X, const ArrayXd &y)
{
	unsigned int XDim = X.cols();
	minHP.resize(XDim+2);
	maxHP.resize(XDim+2);

	fill(minHP.begin(), minHP.end(), 0);

	MatrixXd XRange = X.colwise().maxCoeff() - X.colwise().minCoeff();
	for (unsigned int i=0; i<XDim; i++)
		maxHP[i] = XRange(0,i);
	
	double yMean = y.mean();
	VectorXd zeroMeanY = y - yMean;
    //cout<<"zeroMeanY: "<<zeroMeanY<<std::endl;
	double ySTD = sqrt(zeroMeanY.dot(zeroMeanY) / y.rows());
	maxHP[XDim] = ySTD;
	maxHP[XDim+1] = ySTD;
}

vector<double> SampleNewHyperParamInitialPoint(const vector<double> &minHP, const vector<double> &maxHP)
{
	vector<double> sample(minHP.size());
	std::default_random_engine generator(time(0));
	for (unsigned int i=0; i<minHP.size(); i++)
	{
		std::uniform_real_distribution<double> distribution(minHP[i], maxHP[i]);
		sample[i] = distribution(generator);
	}
	return sample;
}

int main(int argc, char *argv[])
{

    if(argc!=5){
        cout<<"Usage: gp_learn_from_files XFile YFile InputInitialHyperParamsFile OutputHyperParamsFile"<<endl;
        return -1;
    }

    string 	XPath(argv[1]), yPath(argv[2]), hp0Path(argv[3]), hpPath(argv[4]);

	MatrixXd X;
	ArrayXd y;
	vector<double> minHP, maxHP;
	ReadMatrixFromFile(XPath, X);
	X.transposeInPlace();
	cout<<"X.size: \n"<<X.rows()<<", "<<X.cols()<<endl;
	ReadVectorFromFile(yPath, y);
	cout<<"y.size: \n"<<y.rows()<<", "<<y.cols()<<endl;
	GetSqrExpHyperParamsLimits(minHP, maxHP, X, y);

    //cout<<"X: \n"<<X<<endl;
    //cout<<"y: \n"<<y<<endl;
/*
	cout<<"minHP: ";
	copy(minHP.begin(), minHP.end(), ostream_iterator<double>( std::cout, "\t"));
	cout<<"\nmaxHP: ";
	copy(maxHP.begin(), maxHP.end(), ostream_iterator<double>( std::cout, "\t"));
	cout<<endl;    
*/  
	try
	{
		cout << "Create GP" << endl;
		// Create a new GP
		GaussianProcess gp(
			//boost::shared_ptr<SummingKernel>(new SummingKernel()),
			boost::shared_ptr<SquaredExponential>(new SquaredExponential(X.cols())),
			boost::shared_ptr<ZeroMean>(new ZeroMean())
		);

		/*boost::shared_ptr<SquaredExponential> squared_exp(new SquaredExponential);
		boost::shared_ptr<Matern3> matern3(new Matern3);
		boost::shared_ptr<Matern5> matern5(new Matern5);
		(dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
														add_cov_func(squared_exp);
		(dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
														add_cov_func(matern3);
		(dynamic_pointer_cast<SummingKernel>(gp.get_cov_func()))->
														add_cov_func(matern5);*/
		// Set sensible initial parameters
        /*
        std::vector<double> hyper_params;
		hyper_params.push_back(1.0);
		hyper_params.push_back(1.0);
		hyper_params.push_back(1.0);
		hyper_params.push_back(3.0);
    */
        MatrixData hp0MD;
        SetMatrixDataFromFile(hp0Path, hp0MD);
	cout << "Hyper Params read" << endl;
        double noise = hp0MD.data.back();
	hp0MD.data.pop_back();
	cout<<"hp0MD.size: \n"<<hp0MD.height<<", "<<hp0MD.width<<endl;
        gp.set_hyper_params(hp0MD.data);
	cout << "Hyper Params settled!"<< endl;

        gp.set_noise(noise);
	cout << "Noise settled!"<< endl;
	// Feed the data to the GP
	gp.set_training_data(X,y);
        cout << "Training data settled! Starting optimization"<< endl;
	// Optimise the hyper parameters
	NLoptOptimiser optimiser;
	optimiser.set_logging_file_name("opt.log");
	optimiser.optimise(gp);
        cout << "Optimization complete"<< endl;

	AbstractCovarianceFunction::ParamVec_t hyperParams;
	for (unsigned int i=0; i<3; i++)
	{
		vector<double> initialHP = SampleNewHyperParamInitialPoint(minHP, maxHP);
	        /*vector<double> initialHPV(&initialHP[0],&initialHP[initialHP.size()-2]);
			cout<<"initialHP: ";
			copy(initialHP.begin(), initialHP.end(), ostream_iterator<double>( std::cout, "\t"));
			cout<<"\ninitialHPV: ";
			copy(initialHPV.begin(), initialHPV.end(), ostream_iterator<double>( std::cout, "\t"));
	        cout<<endl;*/
        	gp.set_hyper_params(std::vector<double>(&initialHP[0],&initialHP[initialHP.size()-1]));
	        gp.set_noise(initialHP[initialHP.size()-1]);
		cout<<"Iteration "<<i<<"\n\tinitial HPs: ";
		copy(initialHP.begin(), initialHP.end(), ostream_iterator<double>( std::cout, "\t"));
		cout<<endl;
		optimiser.optimise(gp);
		hyperParams = gp.get_cov_func()->get_hyper_params();
		hyperParams.push_back(gp.get_noise());
		cout<<"\n\tfinal HPs: ";
		copy(hyperParams.begin(), hyperParams.end(), ostream_iterator<double>( std::cout, "\t"));
		cout<<endl;
	}
		
	/*
		// Create a set of points to perform predictions for
		MatrixXd test_points(201,3);
		test_points.col(0) = ArrayXd::LinSpaced(201, -7.5f, 7.5f);
		test_points.col(1) = ArrayXd::LinSpaced(201, -7.5f, 7.5f);
		test_points.col(2) = ArrayXd::LinSpaced(201, -7.5f, 7.5f);

		// Get the predictions from the GP
		gp.set_query_points(test_points);
		GPResult output = gp.inference();

		// Write predictions to the harddisk for plotting
		ofstream pred("./pred.dat");
		pred << test_points.col(0).transpose() << endl;
		pred << output.mean.transpose() << endl;
		pred << output.variance.transpose() << endl;
	*/
		ofstream pred("./pred.dat");
		
		
		WriteVectorToFile(hyperParams, hpPath);
	}
	catch (BaseError& e)
	{
		cout<<"Program terminated with exception:\n"<<e.error()<<endl;
	}
    return 0;
}
