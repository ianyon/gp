#include <iostream>
#include <fstream>
#include <nlopt.hpp>

using namespace std;

//! Very simple example that minimises
//! (x-4)^2+1. Using a derivative free
//! algorithm.

double myfunc( const std::vector<double> &x,
                std::vector<double> &grad,
                void *my_func_data )
{
    return pow(x[0]-4,2)+1;
}

int main(int argc, char *argv[])
{

    cout<<"Very simple example that minimises"<<endl;
    cout<<"f(x) = (x-4)^2+1. Using a derivative free"<<endl;
    cout<<"algorithm:"<<endl;

    nlopt::opt opt(nlopt::LN_COBYLA, 1);

    opt.set_min_objective(myfunc, NULL);

    opt.set_xtol_rel(1e-4);

    std::vector<double> x(1);
    x[0] = 1.234;
    double minf;


    nlopt::result result = opt.optimize(x, minf);

    if (result < 0)
    {
        cout<<"Nlopt failed!"<<endl;
    } else
    {
        cout<<"Found result: f("<<x[0]<<") = "<<minf<<endl;
    }


    return 0;
}
