#include <limits>

#include <gp/gp/error.hpp>
#include <gp/util/clenshaw_curtis_quadrature.h>


namespace gp
{

ClenshawCurtisQuadrature::ClenshawCurtisQuadrature(double order)
    :   m_alpha(0.1)
      , m_order(order)
      , m_nodes(new double[m_order])
      , m_weights(new double[m_order])
{
    clenshaw_fill_helper(m_nodes.get(), m_weights.get(), m_order);
}

ClenshawCurtisQuadrature::ClenshawCurtisQuadrature(ClenshawCurtisQuadrature const& c)
    :   m_alpha(c.m_alpha)
      , m_order(c.m_order)
      , m_nodes(new double[m_order])
      , m_weights(new double[m_order])
{
    memcpy(m_nodes.get(), c.m_nodes.get(),m_order);
    memcpy(m_weights.get(), c.m_weights.get(),m_order);
}

double ClenshawCurtisQuadrature::integrate(
        double                  lower_bound,
        double                  upper_bound,
        unary_function_ptr      f
)
{
    if(std::isnan(lower_bound) || std::isnan(upper_bound))
    {
        throw ParameterError("Invalid numeric bounds provided");
    }

    bool negate = (lower_bound > upper_bound);
    if (negate)
    {
        std::swap(lower_bound, upper_bound);
    }

    if ((-std::numeric_limits<double>::infinity() == lower_bound) && (std::numeric_limits<double>::infinity() == upper_bound))
    {
        return negate ? -integrate_inf_to_inf(f) : integrate_inf_to_inf(f);
    }
    else if (-std::numeric_limits<double>::infinity() == lower_bound)
    {
        return negate ? integrate_a_to_inf(lower_bound, f) : -integrate_a_to_inf(lower_bound, f);
    }
    else if (std::numeric_limits<double>::infinity() == upper_bound)
    {
        return negate ? -integrate_a_to_inf(lower_bound, f) : integrate_a_to_inf(lower_bound, f);
    }
    else
    {
        return negate ? -integrate_a_to_b(lower_bound, upper_bound, f) : integrate_a_to_b(lower_bound, upper_bound, f);
    }
}

double ClenshawCurtisQuadrature::double_integrate(
        double                  lower_bound1,
        double                  lower_bound2,
        double                  upper_bound1,
        double                  upper_bound2,
        binary_function_ptr     f
)
{
    Eigen::Map<Eigen::VectorXd> nodes(m_nodes.get(), m_order);
    Eigen::Map<Eigen::VectorXd> weights(m_weights.get(), m_order);

    return (upper_bound1-lower_bound1)/2*weights.transpose()* (lower_bound1+(upper_bound1-lower_bound1)/2*(nodes.array() + 1)).replicate(1,m_order).matrix().binaryExpr((lower_bound2+(upper_bound2-lower_bound2)/2*(nodes.array() + 1)).transpose().replicate(m_order,1).matrix(),f) * (upper_bound2-lower_bound2)/2*weights;
}


void ClenshawCurtisQuadrature::set_order(unsigned int order)
{
    m_order = order;
    m_nodes.reset(new double[m_order]);
    m_weights.reset(new double[m_order]);
    clenshaw_fill_helper(m_nodes.get(), m_weights.get(), m_order);
}

double ClenshawCurtisQuadrature::integrate_inf_to_inf(unary_function_ptr f)
{
    Eigen::Map<Eigen::VectorXd> nodes(m_nodes.get(), m_order);
    Eigen::Map<Eigen::RowVectorXd> weights(m_weights.get(),m_order);
    return M_PI/2/m_alpha * weights * ((tan(M_PI/2*nodes.array())/m_alpha).unaryExpr(f) * (1 + tan(M_PI/2*nodes.array()).square())).matrix();
}

double ClenshawCurtisQuadrature::integrate_a_to_inf(double a, unary_function_ptr f)

{
    Eigen::Map<Eigen::VectorXd> nodes(m_nodes.get(), m_order);
    Eigen::Map<Eigen::RowVectorXd> weights(m_weights.get(),m_order);
    return M_PI/4 * weights * ((a + tan(M_PI/4*(nodes.array()+1))).unaryExpr(f) * (1 + tan(M_PI/4*(nodes.array() + 1)).square())).matrix();
}

double ClenshawCurtisQuadrature::integrate_a_to_b(double a, double b, unary_function_ptr f)
{
    Eigen::Map<Eigen::VectorXd> nodes(m_nodes.get(), m_order);
    Eigen::Map<Eigen::RowVectorXd> weights(m_weights.get(),m_order);

    return (b-a)/2 * weights * (a+(b-a)/2*(nodes.array() + 1)).unaryExpr(f).matrix();
}

void ClenshawCurtisQuadrature::clenshaw_fill(double nodes[], double weights[], unsigned int order)
{
    if (order > 0)
    {
        nodes[0] = 0;
        weights[0] = 2.0;
        int base = 0;
        for(unsigned int n = 2; n <= order; n++)
        {
            base = (double)(n*(n-1))/2;
            clenshaw_fill_helper(nodes + base, weights + base, n);
        }
    }
}


void ClenshawCurtisQuadrature::clenshaw_fill_helper(double nodes[], double weights[], unsigned int n)
{
    double theta;
    for(unsigned int i = 0; i < n; ++i)
    {
        weights[i] = 1;
        theta = M_PI*(n-1-i)/(n - 1);
        nodes[i] = cos(theta);
        for(unsigned int j = 1; j <= (n-1)/2; ++j)
        {
            weights[i] = weights[i] - ((2*j == (n-1)) ? 1 : 2) * cos(2*j*theta)/(4*j*j-1);
        }
    }

    weights[0] = weights[0]/(double)(n-1);
    for(unsigned int i = 1; i < n - 1; i++)
    {
        weights[i] *= 2/(double)(n-1);
    }
    weights[n-1] = weights[n-1]/(double)(n-1);
}


} /* gp */
