#ifndef __GP_CLENSHAW_CURTIS_QUADRATURE_H__
#define __GP_CLENSHAW_CURTIS_QUADRATURE_H__


#include <boost/scoped_array.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

#include <Eigen/Core>


namespace gp
{

/**
 * \brief Integration of functions using the Clenashaw Curtis Quadrature.
 */
class ClenshawCurtisQuadrature
{
    public:
        typedef boost::function<double (double)> unary_function_ptr;
        typedef boost::function<double (double, double)> binary_function_ptr;

    public:
        /**
         * \brief Creates a new quadrature object of the given order.
         *
         * \param order the order of the integration
         */
        ClenshawCurtisQuadrature(double order=20);

        /**
         * \brief Copy constructor.
         *
         * \param c the object to create the copy from
         */
        ClenshawCurtisQuadrature(ClenshawCurtisQuadrature const& c);

        /**
         * \brief Performs simple integration of the function.
         *
         * \param lower_bound the lower bound of the integration range
         * \param upper_bound the upper bound of the integration range
         * \param f the function to integrate
         */
        double integrate(
                double                  lower_bound,
                double                  upper_bound,
                unary_function_ptr      f
        );
        double double_integrate(
                double                  lower_bound1,
                double                  lower_bound2,
                double                  upper_bound1,
                double                  upper_bound2,
                binary_function_ptr     f
        );

        /**
         * \brief Changes the order of the integration.
         *
         * \param order the new order of the integration
         */
        void set_order(unsigned int order);

    private:
        void clenshaw_fill(double nodes[], double weights[], unsigned int order);
        void clenshaw_fill_helper(double nodes[], double weights[], unsigned int n);
        double integrate_inf_to_inf(unary_function_ptr f);
        double integrate_a_to_inf(double a, unary_function_ptr f);
        double integrate_a_to_b(double a, double b, unary_function_ptr f);

        const double                    m_alpha; // parameter for the variable change t = arctan(alpha * x)
        unsigned int                    m_order;
        boost::scoped_array<double>     m_nodes;
        boost::scoped_array<double>     m_weights;
};

    
} /* gp */

#endif /* __GP_CLENSHAW_CURTIS_QUADRATURE_H__ */
