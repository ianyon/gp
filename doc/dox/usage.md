General Usage
=============

This library makes heavy use of use of C++ templates in order to easily
create the specific combination of a Gaussian Process (GP) desired by
the user. The main object around which everything revolves is the
GaussianProcess object. This object requires two template parameters,
the covariance function and the mean function. These two define the
behaviour of the GP.

Once such a GP object is defined and created it can be used with any of
the other functionalities provided. For example the hyper parameters
can be optimized using any of the provided optimizers or we can perform
classification using the GP.

Defining a new covariance function is simply done by writing a new class
that provides the methods required by the GaussianProcess class. A bare
example is provided by the DummyCovariance class. Another good example
is the well known SquaredExponential class. There is no class hierarchy
involved instead if a method required by the GP is not defined C++'s
template mechanism will complain and fail to compile. The reaseon for
the lack of a typical inheritance structure is that templates and
virtual inheritance don't play along all too well and also that we gain
much more flexibility in this way.
