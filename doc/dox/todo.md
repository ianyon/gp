TODO
====


General
-------
- proper test for the nonstationary kernel
- fix derivative tests for the matern functions
- fix derivative test for the rational quadratic function
- organize the tests better, i.e. one call that runs all of them
- write proper tests for the nonstationary kernel
- come up with a mean function hyper parameter optimization scheme

GaussianProcess
---------------
- append_data() and remove_last_data_point() in the GP class are only used for
  the active sampling, maybe solve this more elegantly
- is the noise parameter a single scalar or could it also be a vector?
- there needs to be more error checking on matrix dimensionalities to ensure
  improper usage gets caught early on rather then exploding sometime later
- MultiClass GP why sorting and then why bubble sort
- MultiClass GP get rid of the stupid index starts at 1 thing


classifiers
-----------
- the multiclass classifier is just a dumb function we probably can get rid of
  that one

covariance_functions
--------------------
- if we could require the cov() calls to be const we would end up with some
  nice strong const-ness things through a lot of places

goal_functions
--------------
- What does Roman mean with his comment in the goal function thing?
- I do not trust any of the implementations, we need to check all of them I
  guess

gp
--
- ActiveSampling is a class for no good reason tbh. It could just as well be
  two functions
- Turn the SummingGP class into a bunch of functions taking a set of GPs and
  test points and then does the computation without caring for class structure

optimisers
----------
- the whole optimiser parameter thing is a pain
  - terrible thing to have enums and then casting them back to ints would be
    better to just have loads of static int variables to use as names
- simulated annealing needs to be better parametrized and get rid of hand
  crafter functions to get random numbers

regression app
--------------
- Check for error when loading 3D data.

integral_kernels
-------------
- Define how to call covariance function between curves,
  points and surfaces.
