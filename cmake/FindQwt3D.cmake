# Attempts to locate QWT3D installation.
# Once done the following variables will be defined:
#
#   QWT_3D_FOUND - System has Knitro
#   QWT_3D_INCLUDE_DIRS - The KNITRO include directories
#   QWT_3D_LIBRARIES - The libraries needed to use Knitro
#   QWT_3D_DEFINITIONS - Compiler switches required for using Knitro

set( QWT_3D_ROOT "" CACHE PATH "Root directory of the QWT3D installation" )
mark_as_advanced( QWT__3D_ROOT )

if(QT4_FOUND)

    find_path(
        QWT_3D_INCLUDE_DIRS
        NAME qwt3d_plot.h
        PATHS ${QWT_3D_ROOT} $ENV{QWT_3D_ROOT}
        PATH_SUFFIXES include/ src/
    )

    set(QWT_3D_NAMES ${QWT_3D_NAMES} libqwtplot3d.so libqwtplot3d.so.0 libqwtplot3d.so.0.2 libqwtplot3d.so.0.2.6)

    find_library(
      QWT_3D_LIBRARIES
      NAME  ${QWT_3D_NAMES}
      PATHS ${QWT_3D_ROOT} $ENV{QWT_3D_ROOT}
      PATH_SUFFIXES lib/
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(
        qwt_3d
        DEFAULT_MSG
        QWT_3D_LIBRARIES QWT_3D_INCLUDE_DIRS
    )

endif(QT4_FOUND)

