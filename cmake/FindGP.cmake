# Tries to find the Gaussian Process library. Once done this will define
#   GP_FOUND - System has Gaussian Process Library
#   GP_INCLUDE_DIRS - The GP include directories
#   GP_LIBRARIES - The libraries needed to use GP
#   GP_DEFINITIONS - Compiler switches required for using GP

# The library currently requires Knitro
find_package( KNITRO REQUIRED )


set( GP_ROOT "" CACHE PATH "Root directory of the GP installation" )
mark_as_advanced( GP_ROOT )

find_path(
    GP_INCLUDE_DIRS
    gp/gp/gaussian_process.hpp
    HINTS ${GP_ROOT} $ENV{GP_ROOT}
    PATH_SUFFIXES src include
)

find_library(
    GP_LIBRARY_DIR
    NAMES libgp_optimisers.a libgp_optimisers.so
    HINTS ${GP_ROOT}/lib $ENV{GP_ROOT}/lib
)

get_filename_component( GP_LIBRARY_DIR ${GP_LIBRARY_DIR} PATH )
set(
    GP_LIBRARIES
    ${GP_LIBRARY_DIR}/libgp_covariance_functions.so
    ${GP_LIBRARY_DIR}/libgp_optimisers.so
    ${GP_LIBRARY_DIR}/libgp_mean_functions.so
    ${GP_LIBRARY_DIR}/libgp_util.so
)


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    gp
    DEFAULT_MSG
    GP_LIBRARIES GP_INCLUDE_DIRS
)

mark_as_advanced( GP_LIBRARY_DIR )
