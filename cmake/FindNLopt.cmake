# Attempts to find the NLopt library.
# (Non-Linear Optimization Library)
# http://ab-initio.mit.edu/wiki/index.php/Main_Page
#

# Once done the following
# variables will be defined:
#
#   NLOPT_FOUND - Non-Linear Optimization Library has been found.
#   NLOPT_INCLUDE_DIRS - The NLOPT include directories
#   NLOPT_LIBRARIES - The libraries needed to use NLopt
#   NLOPT_DEFINITIONS - Compiler switches required for using NLopt


set( NLOPT_ROOT "" CACHE PATH "Root directory of the NLopt installation" )
mark_as_advanced( NLOPT_ROOT )

find_path(
    NLOPT_INCLUDE_DIRS
    NAMES nlopt.hpp
    PATHS ${NLOPT_ROOT} $ENV{NLOPT_ROOT}
    PATH_SUFFIXES include
)

find_library(
    NLOPT_LIBRARIES
    NAMES nlopt_cxx
    PATHS ${NLOPT_ROOT} $ENV{NLOPT_ROOT}
    PATH_SUFFIXES lib
)


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    nlopt
    DEFAULT_MSG
    NLOPT_LIBRARIES NLOPT_INCLUDE_DIRS
)
