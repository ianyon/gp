# Attempts to locate QWT installation.
# Once done the following variables will be defined:
#
#   QWT_FOUND - System has Knitro
#   QWT_INCLUDE_DIRS - The KNITRO include directories
#   QWT_LIBRARIES - The libraries needed to use Knitro
#   QWT_DEFINITIONS - Compiler switches required for using Knitro

set( QWT_ROOT "" CACHE PATH "Root directory of the QWT installation" )
mark_as_advanced( QWT_ROOT )

if(QT4_FOUND)

    find_path(
        QWT_INCLUDE_DIRS
        NAME qwt_plot.h
        PATHS ${QWT_ROOT} $ENV{QWT_ROOT}
        PATH_SUFFIXES include/
    )
 
    find_library(
      QWT_LIBRARIES
      NAME  libqwt.so
      PATHS ${QWT_ROOT} $ENV{QWT_ROOT}
      PATH_SUFFIXES lib/
    )

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(
        qwt
        DEFAULT_MSG
        QWT_LIBRARIES QWT_INCLUDE_DIRS
    )

endif(QT4_FOUND)

