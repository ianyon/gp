if [ -d "build" ]
then
    rm -r build
fi
if [ -f src/CMakeLists.txt.user ]
then
    rm src/CMakeLists.txt.user
fi
mkdir build
cd build
cmake ../src
